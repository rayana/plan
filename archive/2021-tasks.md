# Weekly Priorities - 2021

This file is meant to communicate my priorities on a weekly basis. Here's my [README file](https://gitlab.com/rayana/readme/-/blob/master/README.md).

---

## Backlog of cool things I want to do

<details>
<summary>Click to expand the list 💥</summary>

- [ ] Record a quick video about the peer interview for product designers -- talk about what they should look for during the interviews, how to/the importance of reviewing the candidate’s project, tips for when filling out the GH scorecard, and how to collaborate/coach a peer that is shadowing the interview as part of the training. 
- [ ] Blog post or video growth plan
- [ ] Blog post or video PM/UX collab https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68441
- [ ] Handbook updates
  - [ ] Merge Verify UX Pages into one
  - [ ] Delete Release Management UX page
  - [ ] Delete Progressive Delivery UX page
</details>

---

## Next

1. [OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756)
    1. [ ] Calculate MR volume for upcoming milestone
1. My career plan issue / goals and priorities
1. [ ] Milestone planning discussion https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/52
1. [ ] Skill Matrix - summarize results
1. [ ] Create SUS review for CICD https://gitlab.com/gitlab-org/ecosystem-stage/team-tasks/-/issues/102
1. [ ] Verify SUS score https://gitlab.com/gitlab-com/Product/-/issues/3456
1. UX Research
    1. [ ] Erika's research plan https://gitlab.com/gitlab-org/ux-research/-/issues/1662
    1. [ ] Verify, Package research https://gitlab.com/gitlab-org/ux-research/-/issues/1610
    1. [ ] Release research https://gitlab.com/gitlab-org/ux-research/-/issues/1516
    1. [ ] Recruiting screener https://gitlab.com/gitlab-org/ux-research/-/issues/1676

---

## December

### Week 52 | Dec 27 - 31

🎄 PTO

### Week 51 | Dec 20 - 24

🤒 Out sick
🎄 PTO: Dec 24 ([coverage issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1821))

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] UXR Pririty for package
1. [x] D/R
1. [ ] Verify planning/ux collab
1. [ ] V. TEDx video https://www.loom.com/share/df3b46910007448ab6ff907d98fef5c1?sharedAppSource=personal_library
1. [ ] Async 1:1s - cheryl, erika, dan etc

### Week 50 | Dec 13 - 17

🏝 PTO: Dec 16 - 17 ([coverage issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1799))

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756)
    1. [ ] Calculate MR volume for upcoming milestone
1. [ ] V. TEDx video https://www.loom.com/share/df3b46910007448ab6ff907d98fef5c1?sharedAppSource=personal_library
1. [x] Identifying Problem and Solution Validation in Verify https://gitlab.com/gitlab-com/Product/-/issues/1755
1. [x] Remind the team about holiday event - and expense
1. [ ] Async 1:1s - cheryl, erika, dan etc

### Week 49 | Dec 6 - 10

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Update PTO issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/1799
1. [OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756)
    1. [x] [Review UX Debt/Scorecard KR status](https://docs.google.com/spreadsheets/d/1Cn4wZgP0x-_CwiYdS-vVm9-NxaVRcrkCzz7mwPYOXsw/edit#gid=0)
    1. [x] Calulate UX Scorecard volume
    1. [ ] Calculate MR volume for upcoming milestones
    1. [x] Check coverage issues and holiday plans https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/58
1. [x] Hackathon: review issues and loop in designers
    1. [x] https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=Hackathon
    1. [x] https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=Hackathon&label_name[]=UX

### Week 48 | Nov 29 - Dec 03

🏝 PTO: 29 Nov - 01 Dec

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756)
    1. [x] [Review UX Debt KR status](https://docs.google.com/spreadsheets/d/1Cn4wZgP0x-_CwiYdS-vVm9-NxaVRcrkCzz7mwPYOXsw/edit#gid=0)
    1. [ ] Calculate MR volume for upcoming milestones
    1. [ ] Check coverage issues and holiday plans
1. [ ] Consider running pipelines for merged results on Draft MRs https://gitlab.com/gitlab-org/gitlab/-/issues/247067#note_701427767 
1. [ ] Milestone planning discussion https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/52
1. [ ] Skill Matrix - summarize results

## November

### Week 47 | Nov 22 - 26

🏝 PTO: 25 - 26

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. UX Research
    1. [ ] Erika's research plan https://gitlab.com/gitlab-org/ux-research/-/issues/1662
    1. [ ] Verify, Package research https://gitlab.com/gitlab-org/ux-research/-/issues/1610
    1. [ ] Release research https://gitlab.com/gitlab-org/ux-research/-/issues/1516
    1. [ ] Recruiting screener https://gitlab.com/gitlab-org/ux-research/-/issues/1676
1. [OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756)
    1. [x] [Review UX Debt KR status](https://docs.google.com/spreadsheets/d/1Cn4wZgP0x-_CwiYdS-vVm9-NxaVRcrkCzz7mwPYOXsw/edit#gid=0)
    1. [ ] Calculate MR volume for upcoming milestones
    1. [ ] Check coverage issues and holiday plans
1. [x] `UX issue` discussion https://gitlab.com/gitlab-org/gitlab/-/merge_requests/74435#note_738631580
1. [x] Direction updates Ops october https://gitlab.com/gitlab-com/Product/-/issues/3250
1. [ ] Consider running pipelines for merged results on Draft MRs https://gitlab.com/gitlab-org/gitlab/-/issues/247067#note_701427767 
1. [ ] Milestone planning discussion https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/52
1. [ ] Skill Matrix - summarize results

### Week 46 | Nov 15 - 19

🦊 16-17: Virtual contribute!

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Performance Calibration session
1. [x] Q4 UX KRs progress https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756
    1. [x] UX Debt, UX scorecard KR
    1. [x] Finalize pre-work for https://gitlab.com/gitlab-org/gitlab-design/-/issues/1778
    1. [x] Confirm categories https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12492
1. [ ] Direction updates Ops october https://gitlab.com/gitlab-com/Product/-/issues/3250
1. [ ] Verify, Package research https://gitlab.com/gitlab-org/ux-research/-/issues/1610
1. [ ] Release research https://gitlab.com/gitlab-org/ux-research/-/issues/1516
1. [ ] Milestone planning discussion https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/52
1. [ ] Consider running pipelines for merged results on Draft MRs https://gitlab.com/gitlab-org/gitlab/-/issues/247067#note_701427767 
1. [ ] Skill Matrix - summarize results

### Week 45 | Nov 8 - 12

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Research insigths for Adam https://gitlab.com/gitlab-org/ux-research/-/issues/1645
1. [ ] Q4 UX KRs progress https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756
1. [x] Performance reviews
1. [x] UX Showcase
1. [ ] Skill Matrix - summarize results
1. [ ] Milestone planning discussion https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/52
1. [x] Use "CI/CD minutes" consistently instead of "pipeline minutes" https://gitlab.com/gitlab-org/gitlab/-/issues/342813
1. [ ] Consider running pipelines for merged results on Draft MRs https://gitlab.com/gitlab-org/gitlab/-/issues/247067#note_701427767 
1. [ ] Verify, Package research https://gitlab.com/gitlab-org/ux-research/-/issues/1610
1. [ ] Release research https://gitlab.com/gitlab-org/ux-research/-/issues/1516
1. [x] Direction updates Ops october https://gitlab.com/gitlab-com/Product/-/issues/3250

### Week 44 | Nov 1 - 5

🏝 PTO Nov 2

1. [ ] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Skill Matrix - summarize and present results
1. [x] GC Slides https://gitlab.com/gitlab-org/gitlab-design/-/issues/1761
1. [ ] Milestone planning discussion https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/52
1. [x] Update tracking issue and communicate Q4 UX KRs with the team https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756
1. [x] Prep performance reviews
1. [ ] Use "CI/CD minutes" consistently instead of "pipeline minutes" https://gitlab.com/gitlab-org/gitlab/-/issues/342813
1. [ ] Consider running pipelines for merged results on Draft MRs https://gitlab.com/gitlab-org/gitlab/-/issues/247067#note_701427767 
1. [ ] Verify, Package research https://gitlab.com/gitlab-org/ux-research/-/issues/1610
1. [x] Collab with Growth
1. [x] UX retro https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/43
1. [x] Testing - Gina's - discussion guide

## October

### Week 43 | Oct 25 - 29

🏝 PTO Oct 29

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Skill Matrix
    1. [x] Review individually
    1. [x] Async team retro https://gitlab.com/gitlab-org/gitlab-design/-/issues/1753
1. [x] Q4 Goals for UX CI/CD - check in with Product
    1. [x] Create tracking issues for Ally https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756
1. [ ] Use "CI/CD minutes" consistently instead of "pipeline minutes" https://gitlab.com/gitlab-org/gitlab/-/issues/342813
1. [ ] Consider running pipelines for merged results on Draft MRs https://gitlab.com/gitlab-org/gitlab/-/issues/247067#note_701427767
1. [x] Table redesign epic Veethika https://gitlab.com/gitlab-org/gitlab/-/issues/340029
1. [ ] Verify, Package research https://gitlab.com/gitlab-org/ux-research/-/issues/1610
1. [x] UX collab with growth https://about.gitlab.com/handbook/product/growth/#how-growth-collaborates-with-other-groups
1. [ ] UX retro https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/43

### Week 42 | Oct 18 - 22

1. [ ] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [ ] Q4 Goals for UX CI/CD
    1. [x] Q4 Hive https://gitlab.com/groups/gitlab-org/-/epics/6871
    1. [x] Ops goals https://gitlab.com/gitlab-com/ops-sub-department/ops-engineering-management/-/issues/29
    1. [x] Share exp with design managers
1. [x] Verify UX issues (sam/jp)
1. [x] Skill matrix for CI/CD https://gitlab.com/gitlab-org/gitlab-design/-/issues/1707 - share results in the nov 1 ux leadership call
1. [ ] Use "CI/CD minutes" consistently instead of "pipeline minutes" https://gitlab.com/gitlab-org/gitlab/-/issues/342813
1. [ ] Consider running pipelines for merged results on Draft MRs https://gitlab.com/gitlab-org/gitlab/-/issues/247067#note_701427767
1. [x] [UX MRs dashboard](https://gitlab.slack.com/archives/CL9STLJ06/p1634208324273600)
1. [ ] Table redesign epic Veethika https://gitlab.com/gitlab-org/gitlab/-/issues/340029
1. [ ] Verify, Package research https://gitlab.com/gitlab-org/ux-research/-/issues/1610
1. [ ] UX collab with growth https://about.gitlab.com/handbook/product/growth/#how-growth-collaborates-with-other-groups
1. [ ] UX retro https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/43

### Week 41 | Oct 11 - 15

🌞 FF Day on Oct 15

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Host UX Showcase APAC https://gitlab.com/gitlab-org/gitlab-design/-/issues/1723
1. [ ] Q4 Goals for UX CI/CD
    1. [ ] Q4 Hive https://gitlab.com/groups/gitlab-org/-/epics/6871
    1. [x] Testing maturify plans https://www.youtube.com/watch?v=4dRkVVS-0s4
    1. [x] Apply severity to issues https://gitlab.com/gitlab-org/gitlab-design/-/issues/1742
    1. [x] https://gitlab.com/gitlab-org/gitlab/-/issues/342553
1. [x] PA headcount for UX https://gitlab.com/gitlab-com/Product/-/issues/3178
1. [ ] Table redesign epic Veethika https://gitlab.com/gitlab-org/gitlab/-/issues/340029
1. [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/342813
1. [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/247067#note_701427767

### Week 40 | Oct 4 - 8

🤒 Out sick

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Release evidence JTBD https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/90605
1. [x] Testing JTBD https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/91050
1. [ ] ~~Q4 KR proposal~~
1. [ ] Q4 Goals for UX CI/CD
    1. [ ] testing maturify plans https://www.youtube.com/watch?v=4dRkVVS-0s4
1. [ ] Table redesign epic Veethika https://gitlab.com/gitlab-org/gitlab/-/issues/340029
1. [x] Compliance training

## September

### Week 39 | Sep 27 - Oct 1

1. [ ] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] R&D Retro https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12442 **29 Sept**
1. [x] Release evidence JTBD https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/90605
1. [x] Testing JTBD https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/91050
1. [ ] Q4 KR proposal
1. [ ] Q4 Goals for UX CI/CD
    1. [ ] testing maturify plans https://www.youtube.com/watch?v=4dRkVVS-0s4
1. [x] R&D Retrospective (look for email)
1. [x] Close Culture Amp action plans
1. [x] Read growth&dev benefit updates https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/90025/diffs#acb427d784a7d4eb5787b3cd1a6abd204bfa31db
1. [ ] Table redesign epic Veethika https://gitlab.com/gitlab-org/gitlab/-/issues/340029
1. [x] DIB Training
1. [ ] Compliance training

### Week 38 | Sep 20 - 24

* 🌞 OOO Sep 24
* Lots of sync meetings this week. Pushing back items that are not part of my priority list:

1. [ ] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] PM scorecard
1. [x] 360 Feedback: JM
1. [x] 360 Feedback: prep review plan 3/3
1. [x] 360 Feedback: deliver results
1. [x] https://gitlab.com/gitlab-org/gitlab-design/-/issues/1717
1. [x] Runner UX iteration with Darren
1. [x] JTBD package MR review - Nadia https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/89341
1. [x] JTBD Release https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/89516
1. [x] UX Scorecard Katie https://gitlab.com/gitlab-org/gitlab/-/issues/340253
1. [ ] Table redesign epic Veethika https://gitlab.com/gitlab-org/gitlab/-/issues/340029

### Week 37 | Sep 13 - 17

🏝 OOO Sep 17

1. [ ] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] PM interview
1. [ ] PM scorecard
1. [x] UX Group Conv https://gitlab.com/gitlab-org/gitlab-design/-/issues/1710
1. [x] 360 Feedback: share reports
1. [ ] 360 Feedback: prep review plan 0/3
1. [x] JTBD package MR review - Nadia https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/89341
1. [x] UX Scorecard Katie https://gitlab.com/gitlab-org/gitlab/-/issues/340253
1. [x] Discussion guide gina https://gitlab.com/gitlab-org/ux-research/-/issues/1557
1. [ ] Table redesign epic Veethika https://gitlab.com/gitlab-org/gitlab/-/issues/340029

### Week 36 | Sep 6 - 10

1. [ ] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [ ] 360 Feedback: Prep review plan 0/3
1. [x] Communicate mid-year comp
1. [x] Communicate MR Review rate goals
1. [x] JTBD package MR review - Nadia https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/89341
1. [x] UX Scorecard Gina
1. [ ] UX Scorecard Katie https://gitlab.com/gitlab-org/gitlab/-/issues/340253
1. [x] Discussion guide gina https://gitlab.com/gitlab-org/ux-research/-/issues/1557
1. [ ] Table redesign epic Veethika https://gitlab.com/gitlab-org/gitlab/-/issues/340029
1. [ ] UX Group Conv https://gitlab.com/gitlab-org/gitlab-design/-/issues/1710
1. [x] PM Interview
1. [x] PA team meetings
1. [x] NP DB

## August

### Week 35 | Aug 30 - Sep 3

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] 360 Feedback reviews
1. [ ] UX Scorecard Gina
1. [x] Katie's onbording  https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/3062
1. [ ] NP DB

### Week 34 | Aug 23 - 27

🌞 FF Day Aug 27

1. [ ] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Communicate ux scorecard changes
1. [x] Interview PD Import
1. [x] Interview PM CI - no show
1. [ ] Start 360 Feedback reviews
1. [x] UX Scorecard Gina
1. [x] UX Scorecard Daniel
1. [x] KM
   1. [x] Onboarding issue  https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/3062
   1. [x] Inform Package team
   1. [x] Prep UX Buddy - Nadia
   1. [x] Onboarding plan and 1:1 agenda
1. [x] MR wg meeting ([mr](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/87145/diffs))
1. [ ] NP DB
1. [x] **Stretch**: https://gitlab.edcast.com/pathways/feedback-process-at-gitlab 
1. [ ] **Stretch**: https://www.linkedin.com/learning/having-an-honest-career-conversation-with-your-boss/
1. [ ] ~~[Update Runner Enterprise Mgtm epic](https://gitlab.com/groups/gitlab-org/-/epics/4015#iteration-implementation-timeline); ping Darren, Gina...~~

### Week 33 | Aug 16 - 20

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Finish reviewing CI/CD UX Debt issues 
1. [x] Career plan chat with Valerie
1. [x] Review Gina's scorecard
1. [x] Review Daniels scorecard https://gitlab.com/gitlab-org/gitlab-design/-/issues/1670
1. [ ] [Update Runner Enterprise Mgtm epic](https://gitlab.com/groups/gitlab-org/-/epics/4015#iteration-implementation-timeline); ping Darren, Gina...
1. [x] UX Showcase
1. [x] Sync with Cheryl on FE/UX KR for PA
1. [x] Competitor evaluation next steps https://gitlab.com/gitlab-org/gitlab-design/-/issues/1668
1. [ ] Start 360 Feedback reviews
1. [ ] NP DB
1. [ ] **Stretch**: https://www.linkedin.com/learning/having-an-honest-career-conversation-with-your-boss/

### Week 32 | Aug 9 - 13

1. [x] [Weekly: Review pending MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana) 
1. [x] My career development plan
1. [x] Review Gina's scorecard
1. [ ] Review CI/CD UX Debt issues
1. [ ] [Update Runner Enterprise Mgtm epic](https://gitlab.com/groups/gitlab-org/-/epics/4015#iteration-implementation-timeline); ping Darren, Gina...
1. [x] Prep for Package UX updates
1. [x] Sync with Sam on FE/UX KR for PA
1. [x] Individual Growth Plan training
1. [x] UX Showcase - get update from presenters
1. [x] Update SSOT of Package UX issues 
    1. [x] https://gitlab.com/gitlab-org/gitlab/-/issues/327693
    1. [x] https://gitlab.com/gitlab-org/gitlab/-/issues/334583#note_615755399

### Week 31 | Aug 2 - 6

1. [ ] Review Gina's scorecard
1. [x] 360 feedback prep
1. [x] Performance/Growth Training Session
1. [x] Growth & development
    1. [x] Review growth & development plans
    1. [x] Review [Ops Section Cross-Functional Career Development](https://gitlab.com/gitlab-com/Product/-/issues/2233)
1. [ ] Testing/Runner
    1. [x] Handover to Gina
    1. [ ] [Update Runner Enterprise Mgtm epic](https://gitlab.com/groups/gitlab-org/-/epics/4015#iteration-implementation-timeline); ping Darren, Gina...
1. [ ] Package
    1. [x] Align workload with Tim. Onboarding plan for KM.
    1. [x] UX KR Q3
    1. [ ] Headcount issue https://gitlab.com/gitlab-org/gitlab/-/issues/334843
1. [ ] OKRs
    1. [x] Close Q2 UX OKR issue
    1. [ ] ~~Stretch: [Verify shared OKRs Q2](https://gitlab.com/groups/gitlab-com/-/epics/1480) - [Complete async communication training](https://gitlab.com/gitlab-org/verify-stage/-/issues/92)~~
    1. [x] Q3: Verify OKRs https://gitlab.com/gitlab-org/verify-stage/-/issues/89
        1. [x] ~~Communicate FE/UX KR for PA~~
1. [x] UX Showcase prep
1. [ ] Update SSOT of Package UX issues 
    1. [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/327693
    1. [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/334583#note_615755399

## July

### Week 30 | Jul 26 - 30

🌞 OOO July 30

1. [ ] Review pending MRs https://gitlab.com/dashboard/merge_requests?assignee_username=rayana
1. [ ] Hiring Package
    1. [ ] Headcount issue https://gitlab.com/gitlab-org/gitlab/-/issues/334843
1. [x] UX handover issue for Testing/Runner https://gitlab.com/gitlab-org/gitlab-design/-/issues/1657
1. [ ] **OKRs**
    1. [ ] Q2 OKRs
        1. [x] MR widget redesign for Testing Prototype https://gitlab.com/groups/gitlab-org/-/epics/6297
            1. [x] Align with Gina
            1. [x] Get status update from James
        1. [ ] Stretch: [Verify shared OKRs Q2](https://gitlab.com/groups/gitlab-com/-/epics/1480) - [Complete async communication training](https://gitlab.com/gitlab-org/verify-stage/-/issues/92)
    1. [x] Q3:  UX OKRs
        1. [x] Add UX KRs to Ally
        1. [x] Align with GPMs, PMs. Tracking issue, share with them
        1. [x] Align with the team individually and during CI/CD UX
        1. [x] Status update on KR for PA (MR reviews)
    1. [ ] Q3: Verify OKRs https://gitlab.com/gitlab-org/verify-stage/-/issues/89
        1. [ ] Communicate FE/UX KR for PA
1. [x] Runner enterprise mgmt
1. [ ] Update SSOT of Package UX issues 
    1. [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/327693
    1. [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/334583#note_615755399

### Week 29 | Jul 19 - 23

1. [x] Review pending MRs https://gitlab.com/dashboard/merge_requests?assignee_username=rayana
1. [x] Justification KM
1. [x] PM interviews
1. [x] Host UX Showcase
1. [ ] OKRs
    1. [x] MR widget redesign for Testing 
        1. [x] Prototype https://gitlab.com/groups/gitlab-org/-/epics/6297
        1. [x] Review with JH
    1. [ ] Look into Q3 UX OKRs
        1. [ ] Align with GPMs, PMs
        1. [ ] ~~Draft proposal for Valerie on Q3 UX OKR for product categories~~
    1. [x] Look into Verify Q3 OKRs https://gitlab.com/gitlab-org/verify-stage/-/issues/89
    1. [ ] Stretch: [Verify shared OKRs Q2](https://gitlab.com/groups/gitlab-com/-/epics/1480) - [Complete async communication training](https://gitlab.com/gitlab-org/verify-stage/-/issues/92)
1. [x] Get word on testing headcount
    1. [x] Sync with Valerie
    1. [x] Meeting: Package group maturity target review
1. [ ] UX handover issue for Testing/Runner
1. [ ] Update SSOT of Package UX issues 
    1. [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/327693
    1. [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/334583#note_615755399

### Week 28 | Jul 12 - 16

🎡 PTO on July 12th

1. [ ] Review pending MRs https://gitlab.com/dashboard/merge_requests?assignee_username=rayana
1. [x] Work on Package ux issue https://gitlab.com/gitlab-org/gitlab/-/issues/327693
1. [x] Look into any pending [see coverage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1625) tasks
1. [x] GD
    1. [x] Welcome call GD
    1. [x] Sync with V
    1. [x] Sync up with PMs on onboarding plan once more
1. [x] [Hiring Designer for Package](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1448)
    1. [x] KM: Reach out to all 3 reference checks
    1. [x] Update team on hiring status
    1. [ ] Justification / offer
1. [x] PD NS
    1. [x] Inform and get a status update
    1. [x] Submit in BHR
1. [ ] UX OKRs
    1. [x] Get team status update on Q2 OKRs
    1. [ ] Look into Q3 UX OKRs
    1. [ ] Draft proposal for Valerie on Q3 UX OKR for product categories
1. [ ] Look into Verify Q3 OKRs https://gitlab.com/gitlab-org/verify-stage/-/issues/89
1. [ ] Stretch: [Verify shared OKRs](https://gitlab.com/groups/gitlab-com/-/epics/1480) - [Complete async communication training](https://gitlab.com/gitlab-org/verify-stage/-/issues/92)
1. [x] UX Showcase check in https://gitlab.com/gitlab-org/gitlab-design/-/issues/1647
1. [x] Check offboarding issue IC

### Week 27 | Jul 5 - 9

🎡 PTO from July 5 - 9 ([see coverage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1625)), July 12th

1. [ ] (by Jul 9): Verify Q3 OKRs https://gitlab.com/gitlab-org/verify-stage/-/issues/89
1. [x] Onboarding GD tasks
    1. [x] [People ops oboarding issue](https://about.gitlab.com/handbook/people-group/general-onboarding/#managers-of-new-team-members) - https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2923
    1. [x] Schedule welcome call
1. [x] [Hiring Designer for Package](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1448)
    1. [x] Draft/approve job description
1. [ ] Stretch: [Verify shared OKRs](https://gitlab.com/groups/gitlab-com/-/epics/1480) - [Complete async communication training](https://gitlab.com/gitlab-org/verify-stage/-/issues/92)

## June

### Week 26 | Jun 28 - Jul 2

🛫 Flying to Brazil on July 2nd

1. [x] Review pending PTO tasks (catch up)
1. [ ] New joiner tasks for GD: 
    1. [ ] Schedule welcome call
    1. [x] Arrange ux buddy
1. [x] [Hiring Designer for Package](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1448)
    1. [x] Review prospects from Verify on GH
    1. [x] Async kickoff 
    1. [x] Kickoff with Rupert
    1. [x] Confirm criteria with Tim
    1. [x] Draft/approve job description
1. [ ] [Review pending MRs](https://gitlab.com/dashboard/merge_requests?reviewer_username=rayana)
1. [x] Communicate upcoming PTO and review [coverage tasks](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1625) with folks
1. [x] Create issue for APAC UX Showcase
1. [x] Check in on Testing research with Veethika
1. [x] OKR check in
1. [x] Prep for CICD UX meeting
1. [ ] Stretch: [Verify shared OKRs](https://gitlab.com/groups/gitlab-com/-/epics/1480) - [Complete async communication training](https://gitlab.com/gitlab-org/verify-stage/-/issues/92)

### Week 25 | Jun 21 - 25

PTO

### Week 24 | Jun 14 - 18

🎡 PTO from June 17 ([see coverage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1625))

1. [x] Get vaccinated!!
1. [x] Update and share my coverage issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/1625
    1. [x] Expensify -> Valerie
1. [x] [Hiring Designer for Testing/Runner](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1347) 
    1. [x] Offer updates
    1. [x] Interview/screening
    1. [x] Make decision or [Review KM portfolio](https://gitlab.greenhouse.io/people/79291003002?application_id=86953831002)
1. [ ] [Review pending MRs](https://gitlab.com/dashboard/merge_requests?reviewer_username=rayana)
1. [x] Prep for Runner management sync
1. [ ] Review Package UX needs
1. [ ] Proposal for Runner https://gitlab.com/gitlab-org/gitlab/-/issues/19819

**Stretch:**

1. [ ] Growth and development
     1. [ ] Review [Ops Section Cross-Functional Career Development](https://gitlab.com/gitlab-com/Product/-/issues/2233)
     1. [ ] [Feeback on V growth plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/41)
1. [ ] DB MR/SS

### Week 23 | Jun 7 - Jun 11

1. [ ] [Hiring Designer for Testing/Runner](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1347) (interview/screening)
    1. [x] Reference check calls
    1. [x] Justification
    1. [ ] [Review KM portfolio](https://gitlab.greenhouse.io/people/79291003002?application_id=86953831002)
1. [x] Verify PM interview tasks
1. [x] Offboarding tasks https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2819
1. [x] [Review pending MRs](https://gitlab.com/dashboard/merge_requests?reviewer_username=rayana)
1. [x] Check in Package UX
1. [x] Review the [open issues from the Q1 SUS OKR](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=SUS%3A%3AFY22+Q1+-+Incomplete) and work with the team to help them move these forward.
1. [x] Follow up with James on UX coverage for testing https://gitlab.com/groups/gitlab-org/-/epics/5430
1. [x] Get team/PM status on UX OKRs https://gitlab.com/gitlab-org/gitlab-design/-/issues/16031266
1. [x] Check in call with David
1. [x] Think Big status update announcement
1. [ ] Growth and development
     1. [ ] Review [Ops Section Cross-Functional Career Development](https://gitlab.com/gitlab-com/Product/-/issues/2233)
     1. [ ] [Feeback on V growth plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/41)
1. [ ] DB MR/SS
1. [x] Update my coverage issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/1625

### Week 22 | May 31 - Jun 4

1. [x] [Hiring Designer for Testing/Runner](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1347) (interview/screening)
1. [x] Offboarding tasks
1. [x] [Review pending MRs](https://gitlab.com/dashboard/merge_requests?reviewer_username=rayana)
1. [x] Follow up on DF [expense policy](https://about.gitlab.com/handbook/finance/expenses/#-expense-policy) question
1. [x] Check Package handover issue + check in call with David
1. [x] Follow up with JP on UX coverage for testing https://gitlab.com/groups/gitlab-org/-/epics/5430 - Arrange DRI
1. [x] Get team status on UX OKRs https://gitlab.com/gitlab-org/gitlab-design/-/issues/1603
1. [x] PM/UX interview process https://gitlab.com/gitlab-com/people-group/hiring-processes/-/merge_requests/1266
1. [ ] Growth and development
     1. [ ] Review [Ops Section Cross-Functional Career Development](https://gitlab.com/gitlab-com/Product/-/issues/2233)
     1. [ ] [Feeback on V growth plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/41)
1. [ ] DB MR/SS
1. [x] Update my coverage issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/1625

## May

### Week 21 | May 24 - May 28

🎡 Short workweek: OOO on May 24 and May 28

1. [x] [Hiring Designer for Testing/Runner](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1347) (interview/screening)
1. [x] Feedback on NS PD
1. [x] Check in calls with David
1. [x] [Review pending MRs](https://gitlab.com/dashboard/merge_requests?reviewer_username=rayana)
1. [ ] Re-review Release planning issues and leave feedback
      1. [ ] https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/66, https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/65, https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/66, https://gitlab.com/groups/gitlab-org/-/epics/5612
1. [x] [Draft UX question for PM interview process](https://gitlab.slack.com/archives/CKW6KLB8F/p1620994174331400?thread_ts=1620846877.320600&cid=CKW6KLB8F) / https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Product/Product%20Manager/4-EngineeringManagerInterview.md
1. [-] [Feedback on Daniel's research proposal](https://gitlab.com/gitlab-org/ux-research/-/issues/1411) 
1. [x] Give a status update on Amelia's [training issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6394)
1. [ ] Growth and development
     1. [ ] Review [Ops Section Cross-Functional Career Development](https://gitlab.com/gitlab-com/Product/-/issues/2233)
     1. [ ] [Feeback on V growth plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/41)
1. [ ] DB MR
1. [x] Update my coverage issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/1625

### Week 20 | May 17 - May 21

🚑 Multiple doc's appointments this week

1. [x] Get back to Emily's email, discuss it with VK
1. [x] Work on doc for David
1. [x] [Hiring Designer for Testing/Runner](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1347) (interview/screening)
1. [x] [Review pending MRs](https://gitlab.com/dashboard/merge_requests?reviewer_username=rayana)
     1. [x] Prioritize https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/81967
1. [x] Review Release planning issues and leave feedback
      1. [ ] https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/66, https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/65, https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/66
     1. [x] Release Q2 goals https://gitlab.com/groups/gitlab-org/-/epics/5612 
1. [ ] [Draft UX question for PM interview process](https://gitlab.slack.com/archives/CKW6KLB8F/p1620994174331400?thread_ts=1620846877.320600&cid=CKW6KLB8F)
1. [ ] [Feedback on Daniel's research proposal](https://gitlab.com/gitlab-org/ux-research/-/issues/1411) 
1. [ ] Give a status update on Amelia's [training issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6394)
1. [ ] Growth and development
     1. [ ] Review [Ops Section Cross-Functional Career Development](https://gitlab.com/gitlab-com/Product/-/issues/2233)
     1. [ ] [Feedback on NS promotion doc](https://docs.google.com/document/u/0/d/1d0Tk6usyOJNkNx7HmbtwmA0pNgw2k0Gr6yxRX6mE-fE/edit?fromCopy=true)
     1. [ ] [Feeback on V growth plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/41)
1. [ ] DB MR

### Week 19 | May 10 - May 14

🎡 OOO May 13

- [x] Hiring tasks (interview/screening)
- [ ] Get back to Emily's email, discuss it with VK
- [ ] Review https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78181
- [ ] Review pending MRs
- [ ] Review [Ops Section Cross-Functional Career Development](https://gitlab.com/gitlab-com/Product/-/issues/2233)
- [ ] [Feedback on Daniel's research proposal](https://gitlab.com/gitlab-org/ux-research/-/issues/1411) 
- [ ] Give a status update on Amelia's [training issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6394)
- [ ] DB MR
- [x] Check in with Testing/Runner PMs to align on [Q2 UX OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1603)
- [x] UX Showcase
- [x] Look at the marketing product categories on [this page](https://about.gitlab.com/) and identify the ones we currently do not support with a designer
- [x] [PTO Training](https://gitlab.edcast.com/pathways/pto-training-april)
- [x] DB NS
- [x] TMR email
- [x] Plan upcoming time off, communicate with VK

### Week 18 | May 3 - May 7

🎡 OOO May 5 + 📣 Doctor's appointment this week (multiple)

- [x] Hiring tasks (interview/screening).
- [x] Review pending MRs.
- [x] Transition issue code quality to secure.
- [ ] Review [Ops Section Cross-Functional Career Development](https://gitlab.com/gitlab-com/Product/-/issues/2233)
- [ ] Feedback on Daniel's research proposal https://gitlab.com/gitlab-org/ux-research/-/issues/1411
- [x] Facilitate the CI/CD UX Team retro: Revisit [Q1 OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1501) and [engagement survey](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/32) action plan with the team.
- [x] Chat with Taurie about Amelia's traineeship. 
- [ ] Give a status update on Amelia's [training issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6394).
  - [x] Set up recurring 1:1s with Amelia.
- [ ] Check in with Testing/Runner PMs to align on Q2 UX OKRs.
- [x] Check in with Kevin Chu about Release PM responsibilities and upcoming milestone planning.
- [x] Check in with JP about CI UX priorities and upcoming milestone planning.
- [x] Look into PP for NS.
- [x] Align with Taurie on Amelia's traineeship

## April

### Week 17 | Apr 26 - Apr 30

📣 OOO Apr 27

- [x] Hiring tasks 
- [x] CICD UX OKRs
- [x] Prep for CICD UX team retro - go over Q1 OKRs

### Week 16 | Apr 19 - Apr 23

📣 AFK on Apr 22

- [x] Hiring tasks 
- [x] APAC UX Showcase
- [x] PA Team sync retro

### Week 15 | Apr 12 - Apr 16

📣 OOO Apr 16 (ff day)

1. Recruiting
    1. [x] Interviews
    1. [x] 👉 https://gitlab.com/gitlab-com/people-group/recruiting/-/issues/706
    1. [ ] https://gitlab.com/gl-recruiting/req-intake/-/issues/1345
    1. [ ] https://gitlab.greenhouse.io/plans/4817456002/setup
    1. [ ] https://gitlab.com/gl-recruiting/req-intake/-/issues/1347
1. [x] **Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)**
    1. [x] Testing Kick off
    1. [x] Runner kickoff
1. Manager transition issues
    - [ ] https://gitlab.com/gitlab-com/people-group/Training/-/issues/1064
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2638
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/8971
1. [x] chat with Taurie about Amelia's traineeship

### Week 14 | Apr 5 - Apr 9

📣 OOO Apr 5 (Easter)

1. Recruiting
    1. [ ] 👉 https://gitlab.com/gitlab-com/people-group/recruiting/-/issues/706
    1. [ ] https://gitlab.com/gl-recruiting/req-intake/-/issues/1345
    1. [ ] https://gitlab.greenhouse.io/plans/4817456002/setup
    1. [ ] https://gitlab.com/gl-recruiting/req-intake/-/issues/1347
1. [ ] **Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)**
    1. [ ] runner enterprise management MVC
1. Manager transition issues
    - [ ] https://gitlab.com/gitlab-com/people-group/Training/-/issues/1064
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2638
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/8971
1. [ ] chat with Taurie about Amelia's traineeship

## March

### Week 13 | Mar 29 - Apr 2

📣 AFK on Apr 1. OOO Apr 2 (Easter)

1. Recruiting
    1. [x] Check plan with Valerie
    1. [x] Communicate availability to PMs https://gitlab.com/gitlab-org/gitlab-design/-/issues/1578
    1. [ ] 👉 https://gitlab.com/gitlab-com/people-group/recruiting/-/issues/706
    1. [ ] https://gitlab.com/gl-recruiting/req-intake/-/issues/1345
    1. [ ] https://gitlab.greenhouse.io/plans/4817456002/setup
    1. [ ] https://gitlab.com/gl-recruiting/req-intake/-/issues/1347
1. [ ] **Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)**
    1. [ ] runner enterprise management MVC
1. Manager transition issues
    - [ ] https://gitlab.com/gitlab-com/people-group/Training/-/issues/1064
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2638
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/8971
1. [x] Communicate [internet reimbursement](https://gitlab.slack.com/archives/C03MSG8B7/p1616596605161200) to the team
1. [ ] chat with Taurie about Amelia's traineeship

### Week 12 | Mar 22 - 26

1. Recruiting
    1. [x] open requisition\
    1. [ ] 👉 https://gitlab.com/gitlab-com/people-group/recruiting/-/issues/706
    1. [ ] https://gitlab.com/gl-recruiting/req-intake/-/issues/1345
    1. [ ] https://gitlab.greenhouse.io/plans/4817456002/setup
    1. [x] https://gitlab.com/gitlab-com/people-group/hiring-processes/-/merge_requests/1192
    1. [ ] https://gitlab.com/gl-recruiting/req-intake/-/issues/1347
1. [x] Pajamas button migration https://gitlab.com/gitlab-org/gitlab/-/issues/324800#note_535003592
1. [ ] **Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)**
    1. [x] runner enterprise management sync
    1. [ ] runner enterprise management MVC
1. Manager transition issues
    - [ ] https://gitlab.com/gitlab-com/people-group/Training/-/issues/1064
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2638
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/8971
    - [x] https://gitlab.com/gitlab-com/people-group/Training/-/issues/1063
1. [x] Hiring requirements PD
1. [x] Update team meetings
    1. [x] team power
    1. [x] async reviews
    1. [x] think big
1. [ ] chat with Taurie about Amelia's traineeship

### Week 11 | Mar 15 - Mar 19

📣 FF Day on March 19

1. [ ] **Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)**
1. Manager transition issues
    - [ ] https://gitlab.com/gitlab-com/people-group/Training/-/issues/1064
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2638
    - [x] https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77406
    - [ ] https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/8971
    - [ ] https://gitlab.com/gitlab-com/people-group/Training/-/issues/1063
1. [ ] Hiring requirements PD
1. [x] Recording with Taurie 
1. [x] Harassment training
1. [x] Testing/Runner kickoff recording

---

### Week 10 | Mar 8 - Mar 12

🏙 axecon: 10-11 March

1. [ ] **Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)**
    - [x] Align priorities with James
1. [x] Record sessions on growth & design leadership.
1. [x] Attend axecon.
1. [x] Check status of Release milestone planning.

**Backlog**:
1. [ ] Blog post/recording on personal development plan https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10692
1. [ ] Leave feedback for nadia on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1459#note_487800048
1. [ ] Add handbook information related to https://gitlab.com/gitlab-org/gitlab-design/-/merge_requests/267

### Week 9 | Mar 1 - Mar 5

📣 AFK Mar 4.

1. [x] **Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)**
    - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/239514/
1. [x] Work on doc for Valerie
1. [x] Engagement survey todos https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/32
1. [ ] Blog post/recording on personal development plan https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10692
1. [ ] Leave feedback for nadia on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1459#note_487800048
1. [ ] Add handbook information related to https://gitlab.com/gitlab-org/gitlab-design/-/merge_requests/267

---

## February 

1. [x] `Manager: goal of the month`: plan and start growth conversations with CI/CD designers.
1. [ ] **Retrospective action items:**
    1. [ ] 👣 Document UX Way of Working for Testing & Runner: open MR and discuss it with PMs/JP.
    1. [ ] Work on my personal FY21-Q4 OKRs.
    1. [x] Continue working on growth plan with the team. Document lessons learned and action plan for ML.

### Retrospective

<details>
<summary>Click to see the retrospective ☕️ </summary>
</details>

### Week 8 | Feb 22 - Feb 26

📣 Short week. Verify Day on Monday. Growth Day on Thursday. F&F day on Friday.

1. [ ] **Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)**
1. [x] Work on doc for Valerie
1. [x] Verify team day
1. [x] Review product designer's growth plans
1. [x] Create action plan for engagement survey and review it with the team
    1. [x] Engagement survey todos https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/32
1. [ ] Blog post/recording on personal development plan https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10692
1. [ ] Leave feedback for nadia on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1459#note_487800048
1. [ ] Add handbook information related to https://gitlab.com/gitlab-org/gitlab-design/-/merge_requests/267

### Week 7 | Feb 15 - Feb 20

1. [x] UX showcase https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/25
1. [x] https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/47 (deadline Feb 19)
1. [ ] **Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)**
1. [x] Testing kickoff recording
1. [ ] Growth plan:
    1. [x] Give feedback on Iain's plan https://gitlab.com/camuggan/koda/-/merge_requests/1
    1. [x] Create tracking issues for the team, share it with Valerie
1. [ ] Blog post/recording on personal development plan https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10692
1. [ ] Leave feedback for nadia on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1459#note_487800048
1. [ ] Add handbook information related to https://gitlab.com/gitlab-org/gitlab-design/-/merge_requests/267

---

### Week 6 | Feb 8 - Feb 12

📣 I'm mostly AFK on Thursday, Feb 11 (doctor's appointment)

1. [x] Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)
    1. [x] **LOOK INTO ENTERPRISE RUNNER MGMT**
1. [x] Review FY21-Q4 UX OKRs
1. [ ] Growth plan:
    1. [ ] Give feedback on Iain's plan https://gitlab.com/camuggan/koda/-/merge_requests/1
1. [ ] Blog post/recording on personal development plan https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10692
1. [ ] Leave feedback for nadia on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1459#note_487800048
1. [x] Onboarding DF https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2516
1. [x] Check on status of the upcoming UX showcase https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/25
1. [ ] Add handbook information related to https://gitlab.com/gitlab-org/gitlab-design/-/merge_requests/267

---

### Week 5 | Feb 1 - Feb 5

1. [x] `Learning Goal`: Manager Challenge program.
1. [ ] Leave feedback for nadia on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1459#note_487800048
1. [ ] Onboarding DF https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2516
1. [x] Check on status of the upcoming UX showcase https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/25
1. [ ] Add handbook information related to https://gitlab.com/gitlab-org/gitlab-design/-/merge_requests/267
1. [x] Review FY21-Q4 UX OKRs
1. [x] Growth plan:
    1. [x] Prep for Nadia's growth plan call
    1. [x] Prep for Veethika's growth plan call
1. [ ] Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)

---

## January 

1. [ ] `Learning Goal of the month`: [Manager Challenge program](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/) from 2021-01-11 to 2021-01-29. 
    - [Challenge Curriculum](https://docs.google.com/spreadsheets/d/1vRsB80T5vf8r32IxpHU8VvmBrYI_Is0bX8Ckenc0suw/edit#gid=0)
1. [x] `Learning Goal of the month`: Crucial Conversations training from 2021-01-11 to 2021-01-21
    - [Course preparation](https://gitlab.com/gitlab-com/people-group/Training/-/issues/973#course-prep)
1. [ ] `Design Goal of the month`: plan and start growth conversations with CI/CD designers.

### Retrospective

<details>
<summary>Click to see the retrospective ☕️ </summary>

- 👎 January felt like a short month. Catching up after the holidays was tough. I both started and finished the month drawning on unread emails. No time to attend to everything, so I had to continue deprioritizing to make room for more urgent tasks. 
- 👍 I wrote my first [blog post](https://about.gitlab.com/blog/2021/01/05/5-leadership-lessons-as-product-design-manager/) of the year! 
- 👎 I managed to work on some milestone issues for verify, but didn't finish any design proposals. 
   - JH does a great job keeping me in the loop but working mostly async is difficult since I don't have time to go over every item to see what's the latest update. 
   - DE works mostly sync, which is also an issue because we only have so much 1:1 time during the week. 
- 📈 I'll continue using my ux epic to track my priorities, but moving forward I'd like to have a single way of collaborating with PMs so I can be more strategic about my design input/output.
- 👍 I started and finished the crucial conversation training. 
   - 👎 These were two tiring weeks... I had to stay up late because of the training. In all honesty, I didn't have much energy in January. 
- 👍 I started the manager training but I was out sick, so I still have to finish it by the first week of February.
- 👍 Lots of growth and personal conversations with the team this month.
   - 📈 Make sure the team is working on ther personal growth plan on the fly. Continue to encourage them to track their efforts and be more intentional about their contributions at GitLab.
   - 📈 I need to document my own plan for this quarter. I'd be nice to also share the process with the team.
- 👍 DF joined the UX team, which is awesome! I planned his Onboarding tasks around getting to know the product and the operation side of GitLab. I hope he can properly onboard the company so he has higher confidence on how to contribute.
- 👎 I got sick at the end of the month and was ooo for most of week 4.

#### Action items

1. Document UX Way of Working for Testing & Runner: open MR and discuss it with PMs/JP.
1. Work on my personal FY21-Q4 OKRs.
1. Continue working on growth plan with the team. Document lessons learned and action plan for ML.

</details>

---

### Week 4 | Jan 25 - Jan 29

Update: I was out sick for most of the week.

1. [ ] `Learning Goal`: Manager Challenge program.
1. [ ] Leave feedback for nadia on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1459#note_487800048
1. [x] Onboarding DF https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2516
1. [ ] Growth plan:
    1. [ ] Prep for Nadia's growth plan call
    1. [ ] Prep for Veethika's growth plan call
1. [ ] Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)
    1. [ ] [Resolve Test Summary widget research insights](https://gitlab.com/gitlab-org/gitlab/-/issues/293730)
    1. [ ] [[UI/UX] Add option in CI/CD settings for variable expansion](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26346/)
1. [ ] Review the [product development workflow](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/24)

--

### Week 3 | Jan 18 - Jan 22

1. [ ] `Learning Goal`: Manager Challenge program.
1. [x] `Learning Goal`: Crucial Conversations training.
1. [ ] Leave feedback for nadia on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1459#note_487800048
1. [x] Onboarding plan for DF
1. [ ] Growth plan:
    1. [ ] Prep for Nadia's growth plan call
    1. [ ] Prep for Veethika's growth plan call
1. [ ] Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)
    1. [ ] [Resolve Test Summary widget research insights](https://gitlab.com/gitlab-org/gitlab/-/issues/293730)
    1. [ ] [[UI/UX] Add option in CI/CD settings for variable expansion](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26346/)
1. [ ] Review the [product development workflow](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/24)

---

### Week 2 | Jan 11 - Jan 15

- OOO on Friday, Jan 15: Family & Friends day 

1. [x] `Learning Goal`: Manager Challenge program
1. [x] `Learning Goal`: Crucial Conversations training
1. [x] Communicate performance ratings
1. [x] Schedule career growth session with V & N
1. [ ] Staff: [Workflow Design (Verify: Testing & Runner)](https://gitlab.com/groups/gitlab-org/-/epics/5114)
    1. [x] [Resolve Test Summary widget research insights](https://gitlab.com/gitlab-org/gitlab/-/issues/293730)
    1. [ ] [Automated reset of instance wide runner registration tokens (#273090)](https://gitlab.com/gitlab-org/gitlab/-/issues/273090)
    1. [x] [[UI/UX] Add option in CI/CD settings for variable expansion](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26346/)
1. [x] Staff: Prep + Record Testing & Runner kickoff videos
1. [x] Staff: [Watch Testing maturity scorecard update](https://www.loom.com/share/7b7e1cfea62c491cb5fb1beca476a546)

---

### Week 1 | Jan 4 - Jan 8

- 🏥 Doc's oppointment Thursday, Jan 7. Mostly AFK.
- Returning from EOY PTO. Low energy.
- Focused on housekeeping tasks + PDM responsibilities.

1. [x] Create my weekly priorities file
1. [x] `Learning Goal`: Understand GitLab Runners
    1. [x] Watch https://www.youtube.com/watch?v=JFMXe1nMopo - **Need to watch it again, maybe a 3rd time :P**
    1. [x] 1:1 with Steve
1. [x] Create a _Need UX_ epic for Verify:Testing & Runner and share with PMs - https://gitlab.com/groups/gitlab-org/-/epics/5114
1. [x] PDM: [Check status of UX OKRs with the team](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/22)
1. [x] PDM: Write blog post on design manager experience, and share it with Valerie
1. [x] Staff: [Automated reset of instance wide runner registration tokens (#273090)](https://gitlab.com/gitlab-org/gitlab/-/issues/273090)
1. [x] Staff: [Watch Testing maturity scorecard update](https://www.loom.com/share/7b7e1cfea62c491cb5fb1beca476a546)
