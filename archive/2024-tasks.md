# Weekly Priorities - 2024

This file is meant to communicate my priorities on a weekly basis. Here's my [README file](https://gitlab.com/rayana/readme/-/blob/master/README.md).

---

## September

### Week 39

- [ ] Hiring

### Week 38

- [x] V's KRs
- [x] Hiring 
- [x] UX Forum
- [ ] SJ's priorities

### Week 37

- PTO Thu
- FF Day Fri

### Week 36

- [x] SB req 
- [x] Prod division prep
- [x] RCA Support

## August

### Week 35

OOO Aug 28

- [x] RCA
- [x] SB req
- [x] Prod div prep w/ V
- [x] E's transition

### Week 34

- [x] RCA 
- [ ] ~Experience vision PA~
- [x] BoUI 
- [x] Usability week

### Week 33

🤒 Out sick on Monday

- [x] Switchboard UX & coverage
- [x] RCA 
- [ ] Experience vision PA
- [x] Gina's career dev check in
- [x] Q1 MR Labelling Review for SOX Compliance
- [x] FY25 Q2 Navan Expense: Quarterly Manager Sign-off

### Week 32

- [x] People sync
- [x] Switchboard coverage
- [x] RCA 
- [x] BoUI - https://gitlab.com/gitlab-org/gitlab-design/-/issues/2635+

## July

### Week 31

- [x] Parental leave updates V, D
- [x] Switchboard UX
- [x] RCA
- [ ] Q1 MR Labelling Review for SOX Compliance

### Week 30

- [x] Parental leave updates V, D
- [x] E's assignment Plan
- [x] Host UX Forum
- [x] Growth & Dev

### Week 29

- [x] Mid year check in sheet - manager feedback 6/6
- [x] D's leave updates
- [x] Priority calculator
- [x] Switchboard UX

### Week 28

:island: PTO July 4-11

- [x] Catching up
- [x] Mid year check in sheet - manager feedback 

### Week 27

:island: PTO July 4-11

- [x] https://gitlab.com/gitlab-org/gitlab-design/-/work_items/2616+
- [ ] Switchboard UX
- [x] D ux coaching buddy
- [ ] Mid year check in sheet - manager feedback 
- [ ] PA UX ready issues
- [x] Design review: Secrets

## June

### Week 25

- [x] Mid year check in (self)
- [ ] My IGP updates
- [x] AI UX reviews (as needed)
- [x] AI updates
- [x] SP calibration updates
- [x] UX call updates

### Week 25

- [ ] Mid year check in
- [x] Leaves team update
- [x] New performance factor shees [slack](https://gitlab.slack.com/archives/C042XA6VA9E/p1711545199089929)
- [x] My IGP updates
- [x] RCA review with V
- [x] AI UX reviews (as needed)

### Week 24

- [x] UX agenda updates
- [x] Growth & dev conversations
- [ ] My IGP updates
- [x] Code of conduct training
- [x] mid year check in prep ([issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2598))

### Week 23

- [x] Insider training
- [x] SP doc
- [x] AI priorities review and convo
- [x] Secrets manager review
- [x] Verify hackathon
- [x] CI steps sync

## May

### Week 22

GMT+9. OOO May 30-31

- [x] SP doc
- [x] Secrets Manager review

### Week 21

Public Holiday May 20. GMT+9

Plan
- [x] Catching up post PTO
- [x] SP doc
- [x] Expenses sign off
- [x] submit expenses
- [x] ux weekly agenda updates

### Week 20

PTO

### Week 19

PTO

## April

### Week 18

:island: OOO from 30 Apr until 20 May

- [x] JTBD reviews
- [ ] UX Call agenda
- [x] Review coverage issue
- [ ] Switchboard ux planning (initial talks)

### Week 17

- [x] PDoc S
- [x] Japan trip coverage issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/2547
- [x] Skill matrix notes
- [ ] [adoption barriers](https://docs.google.com/presentation/d/13LrEA8YMExsXSQTsQ6fmivj8GH2tIPdm4xOXw-EO3g8/edit#slide=id.g2b14eae506f_0_0)
- [x] JTBD sync + reviews
- [ ] compliance training

### Week 16

- [x] Triage reports
- [ ] Switchboard scorecard https://gitlab.com/gitlab-org/gitlab-design/-/issues/2544

### Week 15

- [x] Review JTBD OKRs https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/102+
- [x] Summarize results https://gitlab.com/gitlab-org/gitlab-design/-/issues/2494+
- [x] Design review
    - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/277391/+
- [x] Team meeting updates
- [x] Sync with marketing - ci adoption
- [x] [Draft: Better CI debugging experience](https://docs.google.com/document/d/1pFm4kRq6UmYEQqUAfuiYBELxjSlF38_bPSekAVgfmxk/edit)
- [x] UX Call - update agenda

## March

### Week 14

OOO Monday and Friday

- [x] UX Weekly updates
- [ ] Review JTBD OKRs https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/102+
- [ ] Summarize results https://gitlab.com/gitlab-org/gitlab-design/-/issues/2494+
- [x] B's IGP
- [x] E's IGP
- Design review
    - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/277391/+

### Week 13

Out sick March 25

- [x] Catching up with emails and todos
- [x] IGP check ins 
- [x] Borrow updates for Emily and V 
- [x] Onboarding/check in B and D 
- [x] Design feedback https://gitlab.com/gitlab-org/gitlab/-/issues/277391+
- [ ] Review JTBD OKRs https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/102+

### Week 12

OOO

### Week 11

Las Vegas Summit 

### Week 10

- [x] V's career check-in
- [x] Onboard DA - ux task and update issue
    - [x] [Switchboard JTBD](https://gitlab.com/gitlab-org/ux-research/-/issues/2383)
- [ ] OKR - review JTBDs https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/102+
- [ ] Actionable insight workshop Conversational AI https://gitlab.com/gitlab-org/ux-research/-/issues/2901#note_1791541307
- [ ] https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/78+
- [x] UX review with Gina
- [x] https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/108+

## February

### Week 9

- [x] Onboard DA 
    - [ ] Support Emily
- [ ] [Switchboard JTBD](https://gitlab.com/gitlab-org/ux-research/-/issues/2383)
- [x] [Borrow req](https://gitlab.com/gitlab-com/Product/-/issues/13128) 
- [ ] https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/78+
- [x] G's IGP check in 
- [x] Coverage issue post Summit
- [x] UXR alignment Karen

### Week 8

- [x] [Finalize DA onboarding plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/100)
- [x] V's career check-in
- [x] E's UX showcase prep
- [x] Review FY25 product direction / [quarterly quickoff](https://university.gitlab.com/learn/video/fy25-q1-quarterly-kickoff)
- [x] [FY24 Q4 Switchboard customer demo insights](https://docs.google.com/presentation/d/1UoAJaDQnZup0TK3zqK0vaKhcAwERiR61dZfPvuEMEKo/edit#slide=id.g12b319f6181_0_0)
- [ ] [Switchboard JTBD](https://gitlab.com/gitlab-org/ux-research/-/issues/2383)
- [x] [Team skill mapping](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2483) | https://gitlab.com/gitlab-org/gitlab-design/-/issues/2494+
- [x] FY25 CICD usability improvements ([slack](https://gitlab.enterprise.slack.com/archives/C042XA6VA9E/p1707843638022719))

### Week 7

- [x] [DA onboarding plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/100)
- [x] OKRs
- [x] https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/97+
- [x] [IGP FY25](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/99)
- [ ] Review FY25 product direction
- [x] https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/1905+
- [x] UX call agenda update

### Week 6

- [x] Hiring
    - [x] Ref checks
    - [x] Justification
    - [x] [Weekly team update](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/2109#note_1660742460)
- [x] IGP prep https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/97+
- [ ] DA Onboarding - ux plan
- [x] OKR review


## January

### Week 5

(my god, how can it STILL be January?)

- [x] Hiring
    - [x] Interviews 0/0
    - [x] Submit scorecards 1/1
    - [x] Sync with Riley
    - [x] Ref check prep
    - [x] Review applications
    - [x] [Weekly team update](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/2109#note_1660742460)
- [ ] ~[OKR: UX Scorecard](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4490)~
- [x] Finalize TA prep 4/4
- [x] [Talent assessment results](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2385)
- [x] G's career dev check in
- [x] [V's IGP](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/75#note_1688665160)

### Week 4

- [ ] Hiring
    - [ ] Interviews 2/3
    - [ ] Submit scorecards 2/3
    - [ ] [Weekly team update](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/2109#note_1660742460)
- [ ] [OKR: UX Scorecard](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4490)
- [x] CI Direction think big
- [x] Merge trains competitor eval https://www.youtube.com/watch?v=eUxNVmi81d8
- [x] Register for Summit

### Week 3

- [x] Hiring
    - [x] Hiring manager tasks
    - [x] Review role req with team
    - [x] Weekly team update
- [x] [Annual comp review](https://handbook.gitlab.com/handbook/total-rewards/compensation/compensation-review-cycle): Submit slates in Workday (due 2024-01-16 @ 5pm PT)
- [x] [OKR: UX Scorecard](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4490) - pick jtbd
- [x] Showcase schedule
- [ ] Register for Summit

### Week 2

- [x] Hiring tasks
- [x] Switchboard role debrief & sync with Riley [doc](https://docs.google.com/document/d/1CEvUJoq7VbkCBTCekJPuaI4XrDM1v08Ze-CAZHiQKdQ/edit)
- [ ] Triage reports
- [ ] [Annual comp review](https://handbook.gitlab.com/handbook/total-rewards/compensation/compensation-review-cycle)
    - [ ] Submit slates in Workday (due 2024-01-16 @ 5pm PT)
- [x] Plan Talent assessment communication (due 2024-02-09)
- [ ] Register for Summit
- [x] Finalize ux Showcase schedule

### Week 1

- [x] Hiring tasks - candidate outreach, riley
- [x] Review Bonnie's milestone plan
- [x] UX Showcase schedule https://gitlab.com/gitlab-org/gitlab-design/-/issues/2448
- [x] Triage reports
