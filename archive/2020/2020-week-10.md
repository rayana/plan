# Week 10 | March 2 - 6

12.9 - 12.10

## Release Management - 12.9

- [ ] User interviews for Releases protoypes https://gitlab.com/gitlab-org/ux-research/issues/754
  - [x] conduct last interview
  - [ ] document insights
  - [ ] update prototypes and create issues
- [ ] **Issues that need UX attention** https://gitlab.com/groups/gitlab-org/-/epics/2439
  - [ ] deploy freeze - update SSOT https://gitlab.com/gitlab-org/gitlab/issues/24295
  - [x] Runbooks for releases - update SSOT https://gitlab.com/gitlab-org/gitlab/issues/9427
  - [x] Support create release data https://gitlab.com/groups/gitlab-org/-/epics/2711
- [ ] JTBD for releases https://gitlab.com/gitlab-org/gitlab/issues/195835 | Look at https://gitlab.com/gitlab-org/uxr_insights/issues/933

## Cross-department

- [ ] https://gitlab.com/gitlab-org/gitlab/issues/26777#note_293384587
- [x] https://gitlab.com/gitlab-org/gitlab/-/merge_requests/22629
- [x] https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/1724

## UX

- [ ] ux scorecards https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/41210
- [ ] ci/cd ux pilot https://gitlab.com/gitlab-org/gitlab-design/issues/866
- [ ] Schedule session to discuss ux environment variables, dimitrie, mike, juan

### Misc

- [ ] Update my OKRs https://gitlab.com/rverissimo/tasks/blob/master/OKR.md
- [ ] read career leadder https://gitlab.com/gitlab-com/www-gitlab-com/issues/6447
- [ ] think big ci/cd https://gitlab.com/gitlab-org/gitlab-design/issues/938
