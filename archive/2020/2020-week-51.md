# Week 51 | Dec 14-18 2020

- Family & Friends day on Friday, Dec 18
- AFK, every Thursday afternoon until February 2021

## General

- [ ] Blog post https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68441
- [x] UX Holiday Party
- [x] UX and planning: chat with Pedro

## Product Design Manager

- [-] **By Dec 15:** UX Creative matrix https://app.mural.co/t/gitlab2474/m/gitlab2474/1607019154360/9f1f9518efa98d0cf6dc38633ed36f44ead34dc1
- [x] Review Veethika's blog post https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70242
- [ ] Prep for perfomance factor rating reviews
- [x] Check status: UX OKRs https://gitlab.com/gitlab-org/verify-and-release-ux-team/-/issues/22
- [x] 1:1s, team calls

## Staff Designer

- [x] K8 Agent vs Runner call
- [ ] `Ongoing`: Onboarding Testing https://gitlab.com/gitlab-com/Product/-/issues/1687
- [ ] `Ongoing`: Onboarding Runner https://gitlab.com/gitlab-com/Product/-/issues/1685 
- [ ] Handbook updates
  - [x] Follow up: Create Runner UX page
  - [ ] Follow up: Create Testing UX page
  - [ ] Follow up: Delete Release Management UX page
  - [ ] Follow up: Delete/Migrate Progressive Delivery UX page
- [x] Runner JTBD page
- [ ] Move this JTBD to another Verify group https://gitlab.slack.com/archives/C0SFP840G/p1607528129244700
