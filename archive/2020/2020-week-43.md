# Week 43 | Oct 19 -23

## General

- [ ] Nadia's transition issue https://gitlab.com/gitlab-org/gitlab/-/issues/263140
- [ ] Acting manager tasks https://gitlab.com/gitlab-com/people-group/Training/-/issues/927

## Release Management

- [ ] **NEED UX REVIEW ITEMS** https://gitlab.com/groups/gitlab-org/-/epics/2439

## GitLab Design

- [ ] Pedro/Taurie maintainer areas
  - [ ] share feedback from Juan
