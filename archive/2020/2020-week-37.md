# Week 37 | Sep 7 - Sep 11

- UX Coverage tasks for Dimitrie until Sep 11 - https://gitlab.com/gitlab-org/gitlab-design/-/issues/1339

## Review MRs

- [ ] https://gitlab.com/dashboard/merge_requests?assignee_username=rayana

## Release Management

- [ ] **UX SHOWCASE VIDEO RECORDING**
- [x] CMS interviews
- [ ] **Issues that need UX attention** https://gitlab.com/groups/gitlab-org/-/epics/2439 
  - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/201959
    - [x] Pajamas https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/695
  - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/230983
- [x] Update [3-year vision mocks](https://gitlab.com/groups/gitlab-org/-/epics/3825)

## Misc

- [x] **SUBMIT EXPENSE REPORT** ov to rotterdam
- [x] **COMPA SHEET**
- [ ] **Settings UX**
  - [x] Schedule call with Secure team 
  - [x] Make plan for async recording and share it with Nadia
  - [ ] Create doc/content for current insights and share it broadly
- [x] UX OKRs
  - [x] Add mirgation issues to RM planning and assign a milestone https://gitlab.com/groups/gitlab-com/-/epics/758
- [ ] PTO: Sync up with Juan - call scheduled

## Next

- [ ] Identify insight themes on Dovetail https://dovetailapp.com/projects/5bcc8db6-7c20-4984-b067-df086f512c67/data/b/3cef4e79-3e73-4e30-93a2-46c08ce7504d
- [ ] Make updates to the group releases and ci/cd dash prototypes based on customer feedback
- [ ] Sync up with nathan and live prototype for release notes https://gitlab.com/groups/gitlab-org/-/epics/2285
- [ ] Read Release Tool Comparison - Electric Cloud https://gitlab.com/groups/gitlab-org/-/epics/3917
