# Weekly Priorities - 2022

This file is meant to communicate my priorities on a weekly basis. Here's my [README file](https://gitlab.com/rayana/readme/-/blob/master/README.md).

---

## December

### Week 52 | Dec 26 - 30

- OOO Dec 26, Dec 30

1. [x] Hiring tasks
    1. [x] review source-a-thon list and log onto GH https://gitlab.com/gitlab-com/people-group/talent-acquisition/-/issues/966 
    1. [x] complete gh tasks
    1. [x] blog post issue
1. [x] Update growth & dev issue, remove from sheet
1. [ ] verify benchmarking 
1. [ ] tufts capstone


### Week 51 | Dec 19 - 23

1. [x] workday performance/growth updates
1. [ ] Hiring tasks
    1. [x] interviews
    1. [x] go over GH pipeline
    1. [x] source-a-thon plan https://gitlab.com/gitlab-com/people-group/talent-acquisition/-/issues/966
    1. [x] source-a-thon review
    1. [ ] blog post issue
    1. [ ] open MR about 2 design managers doing join interviews
1. [ ] [Tufts capstone](https://docs.google.com/document/d/1jA93GJCnmLdleKZjSunEqlHv3wSXTjLLvMNq9alXgf0/edit#) - https://gitlab.com/gitlab-org/gitlab-design/-/issues/2178+
1. [x] https://gitlab.com/gitlab-com/Product/-/issues/5201+
1. [x] ~growth & dev~ 
    1. [x] ~submit expenses~
1. [ ] PA benchmarking tasks https://gitlab.com/gitlab-org/ux-research/-/issues/2281+ by dec 22
---
1. [x] SUS approach for Verify in Q4 - [recap video](https://gitlab.slack.com/archives/C0SFP840G/p1670607370316499)
1. [x] [make talent assessment updates](https://gitlab.slack.com/archives/C01SFG8EDGA/p1670882185068759?thread_ts=1670560683.377539&cid=C01SFG8EDGA)

### Week 50 | Dec 12 - 16

1. [x] [Tufts capstone](https://docs.google.com/document/d/1jA93GJCnmLdleKZjSunEqlHv3wSXTjLLvMNq9alXgf0/edit#) - https://gitlab.com/gitlab-org/gitlab-design/-/issues/2178+
1. [ ] Hiring tasks
    1. [x] interviews
    1. [ ] blog post
    1. [ ] open MR about 2 design managers doing join interviews
1. [x] Promotion plan docs - finalize and share it with Valerie/Marcel
1. [ ] growth & dev
    1. [ ] submit expenses
1. [ ] SUS approach for Verify in Q4 - [recap video](https://gitlab.slack.com/archives/C0SFP840G/p1670607370316499)
1. [ ] https://gitlab.com/gitlab-com/Product/-/issues/5201+
1. [ ] source-a-thon plan
1. [ ] [release post](https://gitlab.slack.com/archives/CPQT50BFG/p1670875833416869), by dec 15
1. [ ] [make talent assessment updates](https://gitlab.slack.com/archives/C01SFG8EDGA/p1670882185068759?thread_ts=1670560683.377539&cid=C01SFG8EDGA)
1. [x] Sunjung's transition meeting 1:1:1

### Week 49 | Dec 5 - 9

1. [x] Hiring tasks
    1. [x] interviews
1. [x] Promotion plan doc - share w Valerie
1. [x] SJ onboarding
    1. [x] align with marcel
1. [x] Emily - review research plan
1. [ ] growth & dev
    1. [ ] submit expenses
1. [x] review promotion docs

## November

### Week 48 | Nov 28 - Dec 2

1. [x] Talent assessment - check if all tasks are done
1. [ ] Hiring tasks
    1. [x] interviews
    1. [x] PI job
    1. [x] PA - SJ transition plan, coverage
1. [x] Q4 OKRs
1. [x] Promotion plan updates
1. [ ] Emily - review research plan
1. [ ] growth & dev
    1. [ ] submit expenses
    1. [ ] ~apply for dec conference~
1. [x] request yubikey

### Week 47 | Nov 21 - 25

1. [x] Hiring tasks
1. [ ] Q4 OKRs
    1. [ ] ~Tracking issue~ not creating it this quarter
    1. [ ] Release usability issues
1. [x] Promotion plan updates

### Week 46 | Nov 14 - 18

- PTO Nov 14 https://gitlab.com/gitlab-org/gitlab-design/-/issues/2138
- FF day Nov 18

1. [x] Hiring tasks
1. [x] Talent assessment
1. [ ] Q4 OKRs
1. [x] GD promotion plan

### Week 45 | Nov 7 - 11

- PTO https://gitlab.com/gitlab-org/gitlab-design/-/issues/2138+

## October

### Week 44 | Oct 31 - Nov 4

- PTO Nov 1 - 4 https://gitlab.com/gitlab-org/gitlab-design/-/issues/2138+

1. [x] Hiring tasks
1. [x] Talent assessment (sheet, doc, workday)
    1. [x] [self](https://docs.google.com/spreadsheets/d/11TGllaZGVkJjrl0IufZXm1dPzIZio1M3JnA4dakaXfI/edit#gid=0)
    1. [x] VM 
1. [x] Update VK on Package UX - slack, add context, plan, recommendation
    1. [x] douglas
1. [ ] Review Katie's mr 

### Week 43 | Oct 24 - 28

1. [x] Hiring tasks
    1. [x] ref checks
    1. [x] justification
    1. [x] interviews
1. [ ] Talent assessment (sheet, doc, workday)
    1. [ ] [self](https://docs.google.com/spreadsheets/d/11TGllaZGVkJjrl0IufZXm1dPzIZio1M3JnA4dakaXfI/edit#gid=0)
    1. [ ] VM 
    1. [x] GD
    1. [x] KM
    1. [x] EB
1. [x] Visiting grant - expenses
1. [x] PTO - coverage issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/2138
1. [ ] Update VK on Package UX - slack, add context, plan, recommendation
    1. [ ] douglas
1. [ ] Review Katie's mr 

### Week 42 | Oct 17 - 21

- Visint Grant, London

1. [x] Hiring tasks
    1. [x] ref checks
1. [x] Align package plan with Viktor
1. [x] Discuss roles with SP, ML
1. [ ] Talent Assessment tasks
    1. [ ] [self](https://docs.google.com/spreadsheets/d/11TGllaZGVkJjrl0IufZXm1dPzIZio1M3JnA4dakaXfI/edit#gid=0)
    1. [x] EB

### Week 41 | Oct 10 - 14

- FF Day, Oct 14

1. [x] Hiring tasks: Verify, Package
1. [ ] release benchmarking https://gitlab.com/groups/gitlab-org/-/epics/7899#note_1112716894
1. [x] UX Transition tasks Package https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/4818
1. [ ] Talent Assessment
    1. [ ] [self](https://docs.google.com/spreadsheets/d/11TGllaZGVkJjrl0IufZXm1dPzIZio1M3JnA4dakaXfI/edit#gid=0)
    1. [x] KM
    1. [ ] EB
1. [x] OKRs update
1. [x] Package UX scope, current, next, UX/FE issues ready for dev

Next:

1. [ ] UX Direction KR plan https://about.gitlab.com/handbook/product/ux/#fy23-direction
1. [ ] CI scaling direction update https://gitlab.com/groups/gitlab-org/-/epics/8597#note_1133257193

### Week 40 | Oct 3 - 7

1. [x] LifeLabs Managing coaching
1. [ ] release benchmarking https://gitlab.com/groups/gitlab-org/-/epics/7899#note_1112716894
1. [x] review MR for gina https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95004#note_1111624041
1. [ ] UX Transition tasks Package (ux roadmap ✅, plan 1:1:1, career mobility doc)
1. [x] loop in V and J in the Secure MR widget proposal
1. [x] Visiting grant travel plan - London
1. [x] Talent assessment training
1. [x] Get started on TA tasks

## September

### Week 39 | sep 26 - 30

- 🌞 FF day - Sep 26

1. [ ] UX Transition tasks Package (ux roadmap ✅, plan 1:1:1, career mobility doc)
1. [ ] loop in V and J in the Secure MR widget proposal
1. [ ] release benchmarking https://gitlab.com/groups/gitlab-org/-/epics/7899#note_1112716894
1. [ ] review MR for gina https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95004#note_1111624041
1. [x] Hiring - interview
1. [x] Hiring PD Package, Verify x2 req
1. [x] Visiting grant travel plan - London
1. [x] Book growth dev travel (flight, hotel)
1. [x] UX showcase
1. [x] Follow up NS offboarding tasks, laptop https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/4776

### Week 38 | Sep 19 - 23

1. [x] https://gitlab.com/gitlab-org/gitlab/-/merge_requests/92780
1. [x] NS offboarding tasks https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/4776
1. [x] PD interview
1. [x] Hiring Tasks
     1. [x] req Verify x2
     1. [x] req Package
1. [x] Package UX plan
     1. [x] Transition
     1. [x] communication plan 
1. [ ] Book growth dev travel
1. [x] UX showcase prep
1. [ ] Visiting grant travel

### Week 37 | Sep 12 - 16

🏙 Black Is Tech Conference: 12-14

1. [x] Nadia's transition plan
1. [ ] Hiring: req Verify x2
     1. [x] Communicate allocation to counterparts
     1. [ ] req criteria
1. [x] Release UX coverage
1. [ ] ~Get started on RCA https://gitlab.com/gitlab-com/gitlab-ux/ux-rca/-/issues/5~
1. [x] PA borrow req https://gitlab.com/gitlab-com/Product/-/issues/4847

### Week 36 | Sep 5 - 9

1. [x] NS CT
1. [x] Verify UX req follow up
1. [x] 360 feedback convo
1. [x] Black is tech conference
1. [x] Notifications sync
1. [x] CI scaling sync
1. [ ] get started on RCA https://gitlab.com/gitlab-com/gitlab-ux/ux-rca/-/issues/5

### Week 35 | Aug 29 - Sep 2

- 🌞 OOO Aug 29

1. [x] 360 reviews
1. [x] Q3 OKRs
1. [x] NS Coaching tasks
1. [x] PA retro
1. [x] Verify UX req
1. [x] https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/109713/diffs

## August

### Week 34 | Aug 22 - 26

- OOO Aug 24-26

1. [x] Q3 OKRs
1. [x] LoA
1. [x] PDM interview
1. [x] ro jtbd
1. [x] bonus
1. [ ] 360 reviews

### Week 33 | Aug 15 - 19

1. [ ] Release roadmap tasks
1. [x] Release Orchestration JTBD
1. [x] Q3 OKRs
1. [x] LoA
1. [ ] ~create coverage issue~
1. [ ] ~PDM interview~
1. [x] london commit
1. [ ] bonus
1. [x] https://levelup.gitlab.com/courses/neurodiversity-in-the-workplace

### Week 32 | Aug 8 - 12

* PTO - Aug 8

1. [ ] Release roadmap tasks
1. [x] Release Orchestration JTBD
1. [x] LoA
1. [x] Q3 OKRs
    1. [x] Manager training
    1. [x] Borrow request
1. [ ] Release UX plan
1. [x] engagement PDM

### Week 31 | Aug 1 - 5

1. [x] Growth and dev check-ins with the team
1. [x] Q3 OKRs: create issue tracker
1. [ ] Release roadmap tasks
1. [ ] Release Orchestration JTBD
1. [x] Review SUS verbatim list for CI/CD https://gitlab.com/gitlab-org/ux-research/-/issues/2040

## July

### Week 30 | Jul 25 - 29

1. [x] Release benchmarking tasks
1. [ ] Release Orchestration JTBD
1. [x] Review runner settings with Gina
1. [ ] Q2 OKRs
    1. [x] Interview questions KR
    1. [-] UX roadmap Release https://gitlab.com/gitlab-org/gitlab/-/issues/364527
1. [ ] Q3 OKRs
    1. [ ] Review SUS KR list

### Week 29 | Jul 18 - 22

* 🏝 PTO - Jul 22

1. [x] NS prioritization plan
1. [ ] search pattern - PI
1. [ ] Q2 OKRs - interview q's
1. [x] Release milestone Emily
1. [x] Environment Management JTBD
1. [x] Usability benchmarking
1. [ ] RCA 
1. [x] OKR alignment
1. [ ] Release UX roadmap
1. [x] Cross-functional prioritization

### Week 28 | Jul 11 - 15

* 🌞 FF Day - July 11

1. [ ] NS prioritization plan
1. [ ] search pattern - PI
1. [x] mid-year check in self assessment
1. [x] usability survey feedback https://docs.google.com/document/d/17p_xFdigTrn6xPHXiANp4ctSIZbk6K94hEx-TbpZsQI/edit#
1. [ ] Q2 OKRs - interview q's
1. [x] release milestone kickoff video

### Week 27 | Jul 4 - 8

1. [ ] RCA https://gitlab.com/gitlab-com/gitlab-ux/ux-rca/-/issues/5
1. [ ] kubecon feedback https://gitlab.com/gitlab-org/ux-research/-/issues/1740
1. [ ] Release UX
    1. [x] jtbd
    1. [x] deliverables
    1. [x] review and give feedback https://about.gitlab.com/handbook/engineering/development/ops/release/planning/#epic-ownership
1. [x] Q2 OKRs - interview q's

### Week 26 | Jun 27 - Jul 1

Catching up after a two-week-long PTO

1. [x] Lifelabs training
1. [ ] RCA https://gitlab.com/gitlab-com/gitlab-ux/ux-rca/-/issues/5
1. [ ] kubecon feedback https://gitlab.com/gitlab-org/ux-research/-/issues/1740
1. [x] Q2 OKRs check in https://gitlab.com/gitlab-org/gitlab-design/-/issues/1978
1. [ ] Release UX
    1. [ ] jtbd Matej
    1. [ ] review and give feedback https://about.gitlab.com/handbook/engineering/development/ops/release/planning/#epic-ownership
1. [ ] cross-functional survey https://gitlab.slack.com/archives/C03MSG8B7/p1656007767638519 (by Friday July 1st)
1. [x] feedback to V on https://gitlab.com/gitlab-org/gitlab/-/issues/363512/

## June

### Week 25 | Jun 20 - 24

* 🏝 PTO

### Week 24 | Jun 13 - 17

* 🏝 PTO - Jun 14-17

1. [x] PTO prep

### Week 23 | Jun 6 - 10

* 🎡 Public holiday - Jun 6

1. [ ] RCA https://gitlab.com/gitlab-com/gitlab-ux/ux-rca/-/issues/new
1. [x] Finalize coverage issue
1. [x] EB onboarding issue and tasks
1. [ ] kubecon feedback https://gitlab.com/gitlab-org/ux-research/-/issues/1740

### Week 22 | May 30 - Jun 3

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks
1. [x] Q2 OKRs
1. [x] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682

## May

### Week 21 | May 23 - 27

* 🎡 Public holiday - May 26
* 🌞 FF Day - May 27

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks - Release UX
1. [x] Q2 OKRs
    1. [ ] Rreview career planning issues
    1. [ ] Share SUS verbatim https://gitlab.com/gitlab-org/ux-research/-/issues/1924#note_936802848
1. [x] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682
    1. [ ] Environments - document latest insights and share
1. [ ] non-mkt JTBD thread https://gitlab.com/gitlab-org/ux-research/-/issues/1907 
1. [ ] verify hackathon https://gitlab.com/gitlab-org/verify-stage/-/issues/203
1. [x] UX retro

### Week 20 | May 16 - 20

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks - Release UX
1. [ ] Q2 OKRs
    1. [ ] Create/review career planning issues
    1. [ ] Share SUS verbatim https://gitlab.com/gitlab-org/ux-research/-/issues/1924#note_936802848
    1. [x] Get status update
    1. [ ] Align with Matej Release JTBD
1. [ ] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682
    1. [x] Organize and document upcoming milestone priorities
    1. [-] Customer call - Environments
    1. [ ] Environments - document latest insights and share
    1. [x] benchmarking part 2 https://gitlab.com/groups/gitlab-org/-/epics/7899
1. [ ] non-mkt JTBD thread https://gitlab.com/gitlab-org/ux-research/-/issues/1907 

### Week 19 | May 9 - 13

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Coaching training
1. [x] Hiring tasks - Release UX
1. [ ] Q2 OKRs
    1. [x] Review SUS KRs
    1. [ ] Create career planning issues
    1. [ ] Share SUS verbatim https://gitlab.com/gitlab-org/ux-research/-/issues/1924#note_936802848
1. [x] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682
    1. [x] Environments interviews
    1. [x] Milestone issues
1. [x] release benchmarking https://gitlab.com/groups/gitlab-org/-/epics/7899
1. [ ] non-mkt JTBD thread https://gitlab.com/gitlab-org/ux-research/-/issues/1907 

### Week 18 | May 2 - 6

* 🎡 Public holiday - May 5
* 🏝 PTO - May 6

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [ ] Q2 OKRs
    1. [x] Create tracking issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/1978
    1. [ ] Review SUS KRs
1. [x] Hiring tasks - Release UX
1. [x] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682
    1. [x] Environments interviews
1. [ ] non-mkt JTBD thread https://gitlab.com/gitlab-org/ux-research/-/issues/1907 
1. [x] categorize SUS verbatim https://gitlab.com/gitlab-org/ux-research/-/issues/1924

## April

### Week 17 | Apr 25 - 29

* 🎡 Public holiday - Apr 27

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks - Release UX
1. [x] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682
1. [x] Q1 OKRs
1. [x] Q2 OKRs

### Week 16 | Apr 18 - 22

* 🎡 Public holiday - Apr 18

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks - Release UX
1. [x] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682
1. [ ] UX review discussion https://gitlab.com/gitlab-org/gitlab-design/-/issues/1936#note_903064412
1. :scissors: :scissors: :scissors: :scissors: :scissors: :scissors:
1. [ ] release team retro https://gitlab.com/gl-retrospectives/release/-/issues/36
1. [ ] ux retro https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/49

### Week 15 | Apr 11 - 15

* 🌞 F&F Day - Apr 11
* 🎡 Public holiday - Apr 15

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks - Release UX
1. [x] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682
1. [x] Release kickoff video
1. [ ] Career Development plan https://gitlab.com/gitlab-org/gitlab-design/-/issues/1896
1. [ ] Offboarding tasks: manager retro
1. :scissors: :scissors: :scissors: :scissors: :scissors: :scissors:
1. [ ] release team retro https://gitlab.com/gl-retrospectives/release/-/issues/36
1. [ ] ux retro https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/49

### Week 14 | Apr 4 - 8

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks - Release UX
1. [x] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682
1. [x] Pajamas Migration Day
1. [ ] Career Development plan https://gitlab.com/gitlab-org/gitlab-design/-/issues/1896
1. [x] PA examples for Valerie
1. [ ] Offboarding tasks: manager retro
1. [x] 👉 Manager training https://gitlab.com/gitlab-org/gitlab-design/-/issues/1866
1. :scissors: :scissors: :scissors: :scissors: :scissors: :scissors:
1. [ ] release team retro https://gitlab.com/gl-retrospectives/release/-/issues/36
1. [ ] ux retro https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/49

## March

### Week 13 | Mar 28 - Apr 1

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks - Release UX
    1. [x] [sourcing](https://about.gitlab.com/handbook/hiring/sourcing/)
1. [x] Release UX: https://gitlab.com/groups/gitlab-org/-/epics/7682
1. [ ] Career Development plan https://gitlab.com/gitlab-org/gitlab-design/-/issues/1896
1. [ ] PA examples for Valerie
1. [ ] Offboarding tasks: manager retro
1. :scissors: :scissors: :scissors: :scissors: :scissors: :scissors:
1. [ ] release team retro https://gitlab.com/gl-retrospectives/release/-/issues/36
1. [x] [Add G&D for ICs to the tracking file](https://docs.google.com/spreadsheets/d/1hLm_XEX3Vux1Co_dMY5A74io8oqXArDAX6MonlBOYNg/edit#gid=0)
1. [x] Pair designer rotation update

### Week 12 | Mar 21 - 25

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks - Release UX
1. [x] Release UX: [transition plan](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1883#currently-ongoing-tasks) | [14.9 milestone](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/106)
1. [ ] Career Development plan
1. [x] [FY23-Q1 OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1841) check in
1. [ ] PA examples for Valerie
1. :scissors: :scissors: :scissors: :scissors: :scissors: :scissors:
1. [ ] Product Keynote at SKO - feedback
1. [ ] Offboarding tasks: manager retro

### Week 11 | Mar 14 - 18

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks - Release UX
1. [x] Release UX: [transition plan](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1883#currently-ongoing-tasks) | [14.9 milestone](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/106)
1. [x] Career Development plan
1. [x] [FY23-Q1 OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1841) check in
1. [ ] PA examples for Valerie
1. [x] UX showcase
1. :scissors: :scissors: :scissors: :scissors: :scissors: :scissors:
1. [ ] Product Keynote at SKO - feedback
1. [ ] Offboarding tasks: manager retro

### Week 10 | Mar 7 - 11

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks
1. [x] Release UX: [transition plan](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1883#currently-ongoing-tasks) | [14.9 milestone](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/106)
1. [ ] Offboarding tasks: manager retro
1. :scissors: :scissors: :scissors: :scissors: :scissors: :scissors:
1. [ ] [FY23-Q1 OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1841) check in
    1. [x] CI/CD engagement survey gitlab-com/www-gitlab-com#12890
    1. [ ] Manager training https://gitlab.com/gitlab-org/gitlab-design/-/issues/1866
1. [ ] Product Keynote at SKO - feedback
1. [ ] PA examples for Valerie
1. [x] 14.9 usability release post https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/100264

## February

### Week 9 | Feb 28 - Mar 4

1. [x] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [x] Hiring tasks
1. [x] Release UX: [transition plan](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1883#currently-ongoing-tasks) | [14.9 milestone](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/106)
1. [x] [Clarify Edcast questions](https://gitlab.edcast.com/pathways/building-empathy-with-our-users/cards/8862124)
1. [ ] Offboarding tasks: manager retro
1. :scissors: :scissors: :scissors: :scissors: :scissors: :scissors:
1. [x] [FY23-Q1 OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1841) check in
    1. [x] CI/CD engagement survey gitlab-com/www-gitlab-com#12890
1. [x] UX showcase prep
1. [x] My career plan issue / goals and priorities https://gitlab.com/gitlab-org/gitlab-design/-/issues/1896

### Week 8 | Feb 21 - 25

🌞 PTO: Feb 25

1. [ ] [Review MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=rayana)
1. [ ] [Clarify Edcast questions](https://gitlab.edcast.com/pathways/building-empathy-with-our-users/cards/8862124)
1. [x] PRC tasks
1. [x] Hiring tasks
1. [x] Prep Growth/Career discussions with the team - next week
1. [x] Configure/Ops design discussion
1. [ ] [FY23-Q1 OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1841)
    1. [x] Managers engagement survey https://gitlab.com/gitlab-org/gitlab-design/-/issues/1836
    1. [ ] CI/CD engagement survey https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12890

### Week 7 | Feb 14 - 18

1. [x] PRC tasks
1. [x] https://gitlab.com/gitlab-org/ux-research/-/issues/1804
1. [x] FY23 Actionable steps https://gitlab.com/gitlab-org/gitlab-design/-/issues/1871
1. [ ] https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97958
1. [x] Finalize compensation announcements
   1. [x] TR about missing DB
1. [x] Close Q4 KR tracker
1. [ ] [FY23-Q1 OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1841)
    1. [ ] Managers engagement survey https://gitlab.com/gitlab-org/gitlab-design/-/issues/1836
    1. [ ] CI/CD engagement survey https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12890

### Week 6 | Feb 7 - 11

🏝 OOO Feb 7 - 9

1. [x] MR reviews
1. [x] PRC tasks
1. [x] Comp changes (1 left)
1. [ ] Close Q4 KR tracker
1. [ ] FY23-Q1 OKRs
    1. [ ] CI/CD UX 
    1. [ ] Managers engagement survey https://gitlab.com/gitlab-org/gitlab-design/-/issues/1836
    1. [ ] CI/CD engagement survey https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12890
1. [x] Release MR reviews alignemnt - done async, need to follow up sync
1. [x] PA alignment

## January

### Week 5 | Jan 31 - Feb 4

1. [x] PRC tasks
1. [ ] Close Q4 KR tracker
1. [x] Prep for next week's PTO
1. [ ] FY23-Q1 OKRs
    1. [ ] CI/CD UX https://gitlab.com/gitlab-org/gitlab-design/-/issues/1841
    1. [x] Alignment with product
    1. [ ] Managers engagement survey https://gitlab.com/gitlab-org/gitlab-design/-/issues/1836
    1. [ ] CI/CD engagement survey https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12890
1. [ ] Monthly: Review UX debt and UX scorecard rec for CI/CD
1. [x] CICD UXR alignment
1. [x] content reflow mr https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2696

### Week 4 | Jan 24 - 28

🚩 Limited availability on Jan 27-28

1. [x] PRC Tasks
1. [x] Share performance results
1. [x] Runner fleet office hours https://gitlab.com/gitlab-org/ux-research/-/issues/1737
1. [ ] FY23-Q1 OKRs prep
    1. [x] Review Ally KRs
    1. [x] Prep tracking issue and communicate to team https://gitlab.com/gitlab-org/gitlab-design/-/issues/1841
    1. [ ] Managers engagement survey https://gitlab.com/gitlab-org/gitlab-design/-/issues/1836
    1. [ ] CI/CD engagement survey https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12890
1. [x] Pair designer schedule https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97517
1. [x] CI/CD pajamas contributions w Jeremy
1. [ ] Monthly: Review UX debt and UX scorecard rec for CI/CD
1. [x] Rotate CICD ux call

### Week 3 | Jan 17 - 21

1. [x] PRC Tasks
1. [x] Compensation review
1. [x] Severity MR https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/96849/diffs
1. [ ] Runner fleet office hours https://gitlab.com/gitlab-org/ux-research/-/issues/1737
1. [ ] Release post tasks https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/96987
1. [ ] FY23-Q1 OKRs prep
    1. [ ] Managers engagement survey https://gitlab.com/gitlab-org/gitlab-design/-/issues/1836
    1. [x] OKR alignment with PMs 4/6
    1. [x] Severity labels https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/59
    1. [x] Severity 2 bugs  https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/60
    1. [ ] CI/CD engagement survey https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12890
1. [x] Release JTBDs

### Week 2 | Jan 10 - 14

🤒 Out sick - Jan 10, 11

1. [x] Catching up
1. [x] Review/approve MRs
1. [x] PRC Tasks
1. [ ] Runner fleet office hours https://gitlab.com/gitlab-org/ux-research/-/issues/1737
1. [ ] Release post tasks https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/96558
1. [x] Verify KRs https://gitlab.com/gitlab-com/Product/-/issues/3649
1. [x] Package quality KRs https://gitlab.com/gitlab-org/quality/triage-reports/-/issues/6009
1. [x] SUS severity https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/59
1. [x] Seveury 2 ux  https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/60
1. [ ] ci policy (veethika) https://gitlab.com/gitlab-org/ux-research/-/issues/1759

### Week 1 | Jan 3 - 7

🏝 PTO
