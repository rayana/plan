# Week 43 | October 21 to 25

* Working Milestone: `12.5`
* Covering for Mike - UX tasks

## What's keeping me busy

- Design OKRs
- Release 12.5 issues, Pajamas MRs, milestone planning

### Tasks

#### UX

- [ ] Review Table styles https://gitlab.com/gitlab-org/gitlab-ui/issues/452
- [ ] Release UX: Productive meetings with stakeholders and counterparts
 https://gitlab.com/gitlab-org/gitlab-design/issues/605
- [ ] Accordion MR https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/merge_requests/1575
- [ ] Tree MR https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/merge_requests/1582

#### Release

[**12.5 Release issue board**](https://gitlab.com/groups/gitlab-org/-/boards/1276625?scope=all&utf8=✓&state=opened&milestone_title=12.5&label_name[]=devops%3A%3Arelease&label_name[]=UX&label_name[]=direction)
- [ ] [Improve detached pipeline - UX Research](https://gitlab.com/gitlab-org/ux-research/issues/244)
    - [ ] Compile results - https://gitlab.com/gitlab-org/ux-research/issues/386
- [ ] Organize issues related to Releases, for planning. Prioritize them with Orit.
 
#### Misc
- [ ] Collect planning feedback results https://gitlab.com/gitlab-org/gitlab/issues/33901
- [ ] Team label process https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28089
- [ ] Clean-up my mail inbox. Goal: 0 unread by the end of the week

### Retrospective

...
