# Week 14 | April 1 to 5

## What's keeping me busy

- [x] Coffee chats: 2
- [ ] Interviews: 0

### Tasks

- [ ] Start [accessibility audit](https://gitlab.com/gitlab-org/gitlab-design/issues/289) for the [Environments Dashboard components](https://gitlab.com/gitlab-org/gitlab-ee/issues/10245) 
  - [ ] Set-up GDK
- [x] Start working on proposal for [Merge trains for merge request pipelines MVC](https://gitlab.com/gitlab-org/gitlab-ee/issues/9186)
- [x] Start working on proposal for [Typed (extensible) environment variables MVC](https://gitlab.com/gitlab-org/gitlab-ce/issues/46806)
- [x] Prepare grooming of [Release 11.11 issues](https://gitlab.com/groups/gitlab-org/-/boards/364216?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Release&milestone_title=11.11&label_name[]=UX) with FE and Dev senior engineers, to make sure UX is working ahead development. [Slack conversation](https://gitlab.slack.com/archives/CBZGR90A2/p1553172953143600).
- [ ] Read [Release's product direction page](https://about.gitlab.com/direction/release/)

### Unplanned tasks

- [x] Create UX Buddy onboarding issue to Mike. Set-up recurring 1:1s
- [x] Create epics for GitLab UI/Pajamas cleanup
- [x] Grooming session for 12.0

### Meetings

Doesn't include recurring company/stage group meetings 😉

- [x] MR Trains discussion

