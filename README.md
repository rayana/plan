# Plan

`Plan` is pretty much a collection of weekly things I am working on at GitLab.

This project started as a way to stay organized while allowing transparency for my teams and anyone interested.

## Who dis?

My name is Rayana and I am a Product Design Manager at GitLab. You can read more about who I am and what I do by checking my personal [README.md](https://gitlab.com/rayana/readme) file.
