# Rayana’s current OKRs

**Overarching goal:** Grow as a product designer by driving innovation across the organization and developing the necessary skills towards design management.

## How this document works

My OKRs work as tool to track my personal/career development at GitLab. Some important things to keep in mind:

* This document helps me strategize short-term efforts for my career development.
* For the sake of [transparency](https://about.gitlab.com/handbook/values/#transparency), my personal OKRs are visible to anyone at the company.
* The OKRs listed below align with both [Product Designer](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/job-families/engineering/product-designer/index.html.md) and [Design Manager](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/job-families/engineering/ux-management/index.html.md) job descriptions. The responsibilities are defined as per the experience factor criteria for: [Sr. Product Designer](https://docs.google.com/spreadsheets/d/1831kFeiTtzqRhsq37wjUv80dVVUSv3YsQYlgx9ExrK0/edit#gid=395345394), [Staff Designer](https://docs.google.com/spreadsheets/d/1EHlEWEJJaZaBmfyUX6QG7SnU5Xh4ak2KtJVSxOXW9po/edit?pli=1#gid=395345394), and [Design Manager](https://docs.google.com/spreadsheets/d/1GdbWo_Orpcxy6JqJA6Oe8cNH2lhtfGArs8pDPH3UxsQ/edit#gid=395345394).
* My archived OKRs can be found [here](https://gitlab.com/rayana/tasks/-/tree/master/OKRs).

## FY21-Q2 / FY21-Q3 OKRs

* Q2: [May 1 through July 31](https://about.gitlab.com/company/okrs/fy21-q2/) | Milestones: 13.0 - 13.2
* Q3: August 1 through October 31 | Milestones: 13.3 - 13.5

### OKR: Become [UX maintainer](https://about.gitlab.com/handbook/engineering/ux/pajamas-design-system/design-review/#maintainer) so I can gain domain and expertise of product design/engineering through Pajamas. `=> 75%`

**Objective:** Increase domain and expertise of product, while being recgonized as an expert in Pajamas and code review.

#### Key Results

1. [x] Conclude the UX Maintainer traineeship process - https://gitlab.com/gitlab-com/www-gitlab-com/issues/4977 `=> 100%, 1/1`
1. [x] Update my status from reviewer to maintainer in the Pajamas project. `=> 100%, 1/1`
1. [x] Announce it on the UX & Release team channels. `=> 100%, 1/1`
1. [ ] Document my experience in a blog post to help other designers become more engaged with the traineeship program. `=> 0%, 0/1`

<details>
<summary>👁 Click to see which responsibilities align with this OKR</summary>

* **Sr. Product Designer responsibility:** Actively contribute to the Pajamas Design System, help determine whether components are single-use or multi-use, and provide recommendations to designers regarding new component requests.					
* **Sr. Product Designer responsibility:** Engage in social media efforts, including writing blog articles, giving talks, and responding on Twitter, as appropriate.

</details>

### OKR: As a UX Maintainer, continously mentor designers by modeling best practices for giving and receving feedback in design contributions. `=> 44%`

**Objective:** Increase domain and expertise of product, while being able to guide and collaborate design decisions.

1. [x] Mentor 1 [trainee maintainer](https://about.gitlab.com/handbook/engineering/ux/pajamas-design-system/design-review/#trainee-maintainer) `=> 100%, 1/1` - [UX Trainee maintainer: Juan](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6608)
1. [ ] `IN PROGRESS` [Improve the `create > build > style` process](https://gitlab.com/groups/gitlab-org/gitlab-services/-/epics/1). `=> 0%, 1/3`
    1. [x] Define goals
    1. [ ] [Deliver solution](https://gitlab.com/groups/gitlab-org/gitlab-services/-/epics/1)
    1. [ ] Communicate to UX department
1. [ ] `IN PROGRESS` Work on improving the Design Trainee Maintainer process by sumitting a MR to the handbook `=> 0%, 0/1`
    1. [ ] https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/429
    1. [ ] https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/45887
1. [ ] `IN PROGRESS` Schedule 5 mentoring sessions with product designers to provide support and clarify personal questions about Pajamas. `=> 80%, 4/5`
    1. [x] Becka
    1. [x] Holly
    1. [x] Iain - https://www.youtube.com/watch?v=xWuebXViqG8 
    1. [x] Amelia
    1. [ ] ...
1. [ ] Every time I complete a new Pajamas component, post it to the UX weekly and Engineering Week-in-Review. `=> 0%, TBD`

<details>
<summary>👁 Click to see which responsibilities align with this OKR</summary>

* **Staff Product Designer responsibility:** Actively contribute to the Pajamas Design System, help determine whether components are single-use or multi-use, and provide recommendations to designers regarding new component requests								
* **Sr. Product Designer responsibility:** Participate in Design Reviews, and model best practices for giving and receving feedback.
* **Sr. Product Designer responsibility:** Mentor other members of the UX department, both inside and outside of your product area.	
* **Staff Product Designer responsibility:** Drive innovation across the organization by driving the adoption of processes and tools.
* **Staff Product Designer responsibility:** Set an example for how to effectively communicate across stages by frequently participating in asynchronous collaboration in issues and merge requests.

</details>
				
---

## OKR: Make the GitLab's UI more usable by burning down Release Management UX Debt issues. `=> 86%`						

**Objective:** Reduce usability issues in our UI with a focus on Release Management.
* **Context:** UX debt label indicates UX resulting from the release does not meet UX and Pajamas specifications or usability and feature viability standards.

### Key Results

1. [x] Identify Release Management UX debt issues from the open backlog ([see issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX%20debt)) and apply the group label. => `1/1 = 100%` [Release Management UX Debt & UI Polish](https://gitlab.com/groups/gitlab-org/-/epics/3416)
1. [x] Work with PM to help prioritize these issues and schedule at least 1 UX debt issue per milestone (13.0 - 13.2). => `200%, 6/3`
1. [x] Burn down at least 10 open cross-stage UX Debt issues related to general UX/usability of GitLab by the end of 13.2. => `60%, 6/10`
1. [ ] Share results and experience broadly with the stage group and UX teams in 1 blog post or 1 async video/asset. `=> 0%, 0/1`

<details>
<summary>👁 Click to see which responsibilities align with this OKR</summary>

* **Sr./Staff Product Designer responsibility:** Understand UX debt and make recommendations for its resolution.
* **Sr. Product Designer responsibility:** Proactively identify both small and large usability issues within your product area, and help influence your Product Design Manager and Product Manager to prioritize them.							
* **Sr. Product Designer responsibility:** Support your Product Design Manager and Product Manager in identifying dependencies between stages and advocating for cross-stage collaboration when needed.

</details>

---

## Share my UX work broadly and frequently to reinforce department and group's vision. `=> 83%`

**Objective:** Continue to socialize UX efforts to gain and share experiences across departments.

### Key Results

1. [ ] Write a blog post with Jackie about partnership between UX/PM. `=> 0%, 0/1`
1. [x] Identify an opportunity to represent GitLab in the external community (event, meetup).`=> 100%, 1/1`
1. [x] Present results of Release Management UX efforts during the UX Showcase. `=> 66%, 2/3`
    1. https://youtu.be/ZIjOSYGlve4
    1. https://youtu.be/BPwlmLEW3FE
1. [x] Share ongoing as well as research results with the CI/CD and Ops UX teams during showcases. `=> 40%, 2/5`
    1. https://youtu.be/u33_pD4NKks
    1. https://youtu.be/kFglNWxFL3k?t=2354
    1. https://youtu.be/HgyKRpyKDT0
1. [x] Share best practices in processes and collaboration workflows with the UX team, either via a working session or recorded presentation. `=> 100%, 1/1`
    1. Shared best practices across the CI/CD UX team, both in 1:1 and team calls. Discussed UX/PM workflows, [Need UX epic](https://gitlab.com/groups/gitlab-org/-/epics/2439), UX DoD, CI/CD ux team workflows etc
    1. https://youtu.be/t7Q5gtgHF8Y
    1. https://youtu.be/XzPPu_SItJ0?t=803
    1. https://youtu.be/992CXrIneH8
    1. https://youtu.be/d2PeZPI4pU8
    1. https://youtu.be/T5kN5EQEskk?t=1017
1. [x] Post results from UX Scorecards and Solution Validation to the Engineering Week-in-Review. `=> 100%, 1/1`
    1. [x] Create a release and update it [see agenda doc](https://docs.google.com/document/d/1EkfzI85aqw8chYDBf2GLRvjKEa3s0FWHMI3u0DIr-xg/edit#bookmark=id.64k5qjfpjb1b)
    1. [x] Secrets Management [see agenda doc](https://docs.google.com/document/d/1EkfzI85aqw8chYDBf2GLRvjKEa3s0FWHMI3u0DIr-xg/edit#bookmark=id.lx210fr2e95n)

<details>
<summary>👁 Click to see which responsibilities align with this OKR</summary>

* **Sr. Product Designer responsibility:** Communicate the results of UX activities within your stage group to the UX department, cross-functional partners within your product area, and other interested GitLab team-members using clear language that simplifies complexity.
* **Staff Product Designer responsibility:** Teach and socialize decision-making frameworks to the GitLab community so that designers can solve problems more efficiently on their own.

</details>

---

## OKR: Cross-stage - Settings UX `=> 100%`

* **Objective:** Improve the Settings UX experience in GitLab by collect existing user feedback and proposals in order to drive user experience and SUS improvements.
* **Context:** Settings falls under not_owned, therefore no designer is currently dedicated to effectively work on improvements towards that product area.

### Key Results

1. [x] Work with Nadia to identify opportunities across stage group, and document current situation of Settings in an epic 
    - https://gitlab.com/groups/gitlab-org/-/epics/3535
1. [x] Define strategy and plan steps with Nadia
1. [x] Evaluate the existing settings UX issues and identify low-hanging fruits - map out issues and assign them to themed epics.
1. [x] Reach out to design teams and identify shared responsibility. Record conversations and share it with the UX Department.
1. [x] Create a comprehensive list of content to work on & timelines - Collectively put together a list of upcoming needs for Settings flows/components/layouts/interactions.
1. [x] Work with UX Leadership to identify DRIs that can implement Settings improvements.


## OKR: Stage group - Help move [Secrets Management](https://about.gitlab.com/direction/release/secrets_management/#maturity-plan) closer to *Viable* by performing a CMS with a score of at least 3.14 `=> 100%`

* **Objective:** Support the Secrets Management direction `Make Release Management easier` by proactively conducting a CMS.
* **Context:** This category is currently at the `Minimal` maturity level, and our next maturity target is `Viable`. 
* **Viable**: Significant use at GitLab the company. UMUX Lite score at least 3.14 for the main job to be done (JTBD) when tested with internal users. No assessment of related jobs to be done.

### Key Results

1. [x] Conduct CMS for Secrets Management in 13.4 `=> 100%, 1/1`
    1. https://gitlab.com/gitlab-org/ux-research/-/issues/1044
1. [x] Create new recommendations based on customer feedback by 13.5 `=> 100%, 1/1`
1. [x] Work with PM to help prioritize the issues by the end of `Q3` `=> 100%, 1/1`
1. [x] Work with PM to update the category vision page by the end of 13.4. `=> 100%, 1/1`
    1. https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/61902
1. [x] Communicate results to the UX and Engineering departments `=> 100%, 2/2`
    1. [x] UX weekly
    1. [x] [Engineering Week-in-Review](https://docs.google.com/document/d/1EkfzI85aqw8chYDBf2GLRvjKEa3s0FWHMI3u0DIr-xg/edit#bookmark=id.lx210fr2e95n)
1. [x] If applicable, identify improvements to the CMS process, and share it broadly with the UX department. `=> 100%, 1/1`
    1. https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/61909

<details>
<summary>👁 Click to see which responsibilities align with this OKR</summary>

* **Sr. Product Designer responsibility:** Independently conduct solution validation with minimal guidance from your Product Design Manager and incorporate insights into design decisions to fulfill user and business needs.
* **Sr./Staff Product Designer responsibility:** Create tactical and strategic deliverables for your product area (for example: wireframes, competitive analyses, prototypes, journey maps, storyboards, personas, etc.) 
* **Staff Product Designer responsibility:** Exert significant influence on the overall objectives and long-range goals of your section by actively contributing to the Vision pages with Product and Engineering.

</details>

---

## OKR: Stage group - Help move [Release Orchestration](https://about.gitlab.com/direction/release/release_orchestration/#maturity-plan) closer to *Complete* with a score of at least 3.63 `=> 0%`

* **Objective:** Support the Release Orchestration direction `Make Release Management easier` by proactively conducting validation.
  * https://gitlab.com/groups/gitlab-org/-/epics/2131
* **Context:** GitLab the company dogfoods it exclusively. At least 100 customers use it. UMUX Lite score at least 3.63 for main JTBD (and related JTBDs, if applicable) when tested with external users.

### Key Results

1. [ ] Conduct solution validation for the usability of Releases - https://gitlab.com/groups/gitlab-org/-/epics/2355 by the end of xx.x. `=> 0%, 1/1`
1. [ ] Create new recommendations and design deliverables for Releases that focus on usability and UI performance  by the end of xx.x. `=> 0%, 1/1`
1. [ ] Work with PM to help prioritize the issues by the end of xx.x. `=> 0%, 1/1`
1. [ ] Work with PM to update the category vision page by the end of xx.x. `=> 0%, 1/1`
1. [ ] Schedule: Revalidate the UX Scorecard and move the [baseline score](https://about.gitlab.com/handbook/engineering/ux/ux-scorecards/#grading-rubric) of the Release capability to at least a **C**. Score at least 3.63 for main JTBD `=> 0%, 1/1`

<details>
<summary>👁 Click to see which responsibilities align with this OKR</summary>

* **Sr. Product Designer responsibility:** Independently conduct solution validation with minimal guidance from your Product Design Manager and incorporate insights into design decisions to fulfill user and business needs.
* **Sr./Staff Product Designer responsibility:** Create tactical and strategic deliverables for your product area (for example: wireframes, competitive analyses, prototypes, journey maps, storyboards, personas, etc.) 
* **Staff Product Designer responsibility:** Exert significant influence on the overall objectives and long-range goals of your section by actively contributing to the Vision pages with Product and Engineering.

</details>

---

## OKR: Career development - Gain experience and develop skills in [design management](https://about.gitlab.com/job-families/engineering/ux-management/) `=> 57%`

**Objective:** Create opportunities to directly experience a leadership role at GitLab by learning and developing the necessary skills to transition into a different career choice.

1. [x] Familiarize myself with the Staff/Design Manager role by reading the [UX Management](https://about.gitlab.com/job-families/engineering/ux-management/) handbook page by the end of April, 2020. `=> 100%, 1/1`
1. [x] Sync up with current Staff designers to understand their experience `=> 100%, 2/2`
1. [x] Align career goals, skils, define and document active learning opportunities areas together with Nadia during a working session, by May 7th, 2020. `=> 100%, 1/1`
1. [ ] Formulate proposal for UX Manager role `Traineeship` or `Shadowing`. Work together with Nadia and present it to Christie by the end of Q2. `=> 0%, 0/1`
    1. https://gitlab.com/gitlab-org/gitlab-design/-/issues/1145
1. [ ] Read the [Learning & Development](https://about.gitlab.com/handbook/people-group/learning-and-development/) material in the handbook: `=> 67%, 2/3`
    1. [x] [Career Development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/)
    1. [x] [Transitioning to a manager role](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-development/)
    1. [ ] [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/)
1. [ ] Schedule a monthly session with Jackie to discuss leadership mentoring and gain knowledge from a cross-functional perspective. By the end of May, 2020. `=> 0%, TBD`
1. [ ] Schedule an online course in design/business management.`=> 0%, 0/2`
    1. [ ] Identify options together with Nadia and get budget approval.
    1. Backlog options: [Design Management - London College of Communication](https://www.arts.ac.uk/subjects/business-and-management-and-science/short-courses/service-design-and-design-thinking/design-management-online-short-course-lcc)
1. [x] Gain broader design skills by completing 1 formal training. `=> 100%, 1/1`
    1. [x] [Comprehensive Introduction to Research Methodology and Design](https://summerschool.uva.nl/content/summer-courses/introduction-to-research-methodology-and-design/introduction-to-research-methodology-and-design.html) - Universiteit van Amsterdam
    1. [-] ~~[Design for Emotion and Happiness](https://www.tudelft.nl/en/ide/education/ide-design-master-classes/design-for-emotion-and-happiness/) - TUDelft~~
    1. [-] ~~[Design Roadmapping](https://www.tudelft.nl/en/ide/education/ide-design-master-classes/design-roadmapping/) - TUDelft~~
